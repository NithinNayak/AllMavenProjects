package com.mindtree.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.entity.Message;

@RestController
@CrossOrigin
@RequestMapping("/rest")
public class HelloWorldRestController {
	@RequestMapping(value = "/hi", method = RequestMethod.POST)
	public List<Message> doSome(@RequestBody Message msg3) {
		System.out.println("GGG");
		Message msg1 = new Message();
		Message msg2 = new Message();
		msg1.name = "Nithin";
		msg1.text = "Hello";
		msg2.name = "Sachin";
		msg2.text = "Hi";
		List<Message> li = new ArrayList<Message>();
		li.add(msg1);
		li.add(msg2);
		li.add(msg3);
		return li;

	}

	/*
	 * @RequestMapping("/hello/{player}") public Message message(@PathVariable
	 * String player) {
	 * 
	 * Message msg = new Message(player, "Hello " + player); return msg; }
	 */
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public ArrayList<Message> doSome() {
		System.out.println("hjkkjhgfhg");
		ArrayList<Message> li=new ArrayList<Message>();
		Message m=new Message();
		m.setName("rahul");
		m.setText("hi");
		li.add(m);
		return li;

	}
}
