package countingbits;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static int[] getOneBits(int n) {
		String binaryStr = "";
		int arrSize = 0;
		while (n != 0) {
			int rem = n % 2;
			arrSize = arrSize + rem;
			binaryStr = binaryStr + rem;
			n = n / 2;
		}
		StringBuffer revStr = new StringBuffer(binaryStr);
		revStr = revStr.reverse();
		int res[] = new int[arrSize + 1];
		res[0] = arrSize;
		int j = 1;
		for (int i = 0; i < revStr.length(); i++) {
			if (revStr.charAt(i) == '1') {
				res[j] = i + 1;
				++j;
			}
		}
		return res;

	}

}
