package countduplicates;
import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {
	static int countDuplicates(int[] numbers) {
		// Write your code here.
		 int count=0;
		 int tmp[]=new int[10000];
		 for(int i=0;i<numbers.length;i++)
		 {
			 ++tmp[numbers[i]];
		 }
		 for(int j=0;j<tmp.length;j++)
		 {
			 if(tmp[j]>1)
				 ++count;
		 }
		 
			return count;
	}
}
