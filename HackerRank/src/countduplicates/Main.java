package countduplicates;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		 int n=scan.nextInt();
		 int numbers[]=new int[n];
		 for(int i=0;i<n;i++)
			 numbers[i]=scan.nextInt();
		 int res = Solution.countDuplicates(numbers);
		 System.out.println(res);

	}

}
