package mergesort;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter your name:");
		String name = scan.next();
		System.out.println("Sorted order is:");
		char res[] = partition(name.toCharArray());//Converting the string 'name' into character array to sort it
		for (char ch : res) {
			System.out.print(ch);
		}

	}

	public static char[] partition(char[] name) {//Divides character array into two halves in a recursive manner till the single character is found(boundary condition)
		int n = name.length;
		if (n > 1) {//This is the recursion termination condition
			char[] left = new char[n / 2];//Stores the left half of array
			char[] right = new char[n - (n / 2)];//Stores right half of the array
			for (int i = 0; i < (n / 2); i++)
				left[i] = name[i];
			for (int j = (n / 2); j < n; j++)
				right[j - (n / 2)] = name[j];
			partition(left);//Gets called recursively till array size becomes 1
			partition(right);//Gets called recursively till array size becomes 1
			merge(name, left, right);
		}
		return name;//Returns sorted 'name' array to main
	}

	public static void merge(char[] name, char[] left, char[] right) {
		int nameIndex = 0, leftIndex = 0, rightIndex = 0;
		while (leftIndex < left.length && rightIndex < right.length) {//Comparing the left and right half of array
			if (left[leftIndex] < right[rightIndex]) {//if elements of left arrays are lesser than the right, then store the left array elements in name array
				name[nameIndex] = left[leftIndex];
				++leftIndex;
			} else {                               //if elements of left arrays are right than the right, then store the right array elements in name array
				name[nameIndex] = right[rightIndex];
				++rightIndex;
			}
			++nameIndex;
		}
		//The following code will store any remaining elements present in right or left after the comparison above comparison
		while (leftIndex < left.length) {
			name[nameIndex] = left[leftIndex];
			++nameIndex;
			++leftIndex;
		}
		while (rightIndex < right.length) {
			name[nameIndex] = right[rightIndex];
			++nameIndex;
			++rightIndex;
		}
	}

}
