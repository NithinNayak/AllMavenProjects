package maximumdifferenceinarray;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static int maxDifference(int[] a) {
		int maxDif = a[1] - a[0];
		int minIndVal = a[0];
		int aIndex = 1;
		while (aIndex < a.length) {
			if (a[aIndex] - minIndVal > maxDif) {
				maxDif = a[aIndex] - minIndVal;
			}
			if (a[aIndex] < minIndVal) {
				minIndVal = a[aIndex];
			}
			++aIndex;
		}
        if(maxDif>0)
		return maxDif;
        else
        	return -1;

	}

}
