package anagram;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

	static int[] getMinimumDifference(String[] a, String[] b) {
		int res[] = new int[a.length];
		for (int i = 0; i < a.length; i++) {
			if (a[i].length() != b[i].length()) {
				res[i] = -1;
			} else {
				char a1[] = mergeSort(a[i].toCharArray());
				char b1[] = mergeSort(b[i].toCharArray());
					for (int j = 0; j < a1.length; j++) {
						res[i] = res[i] + binarySearch(a1, b1[j]);
					}
				}
			}

		return res;
	}

	public static char[] mergeSort(char[] arr) {
		int n = arr.length;
		if (n > 1) {
			int mid = n / 2;
			char[] left = new char[mid];
			char[] right = new char[n - mid];
			for (int i = 0; i < mid; i++)
				left[i] = arr[i];
			for (int i = mid; i < n; i++)
				right[i - mid] = arr[i];
			mergeSort(left);
			mergeSort(right);
			merge(arr, left, right);
		}
		return arr;
	}

	public static void merge(char[] arr, char[] left, char[] right) {
		int l = left.length;
		int r = right.length;
		int i = 0, j = 0, k = 0;
		while (i < l && j < r) {
			if (left[i] <= right[j]) {
				arr[k] = left[i];
				++i;
			} else {
				arr[k] = right[j];
				++j;
			}
			++k;
		}
		while (i < l) {
			arr[k] = left[i];
			++i;
			++k;
		}
		while (j < r) {
			arr[k] = right[j];
			++j;
			k++;
		}
		// return arr;
	}

	public static int binarySearch(char[] arr, char x) {
		int start = 0;
		int end = arr.length - 1;
		while (start <= end) {
			int mid = (start + end) / 2;
			if (arr[mid] == x)
			{arr[mid]=0;
				return 0;
			}
			else if (x < arr[mid])
				end = mid - 1;
			else
				start = mid + 1;
		}
		return 1;
	}

}
