package hacklandelection;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N=in.nextInt();
		String votes[]=new String[N];
		for(int i=0;i<N;i++)
		{
			votes[i]=in.next();
		}
		String res = Solution.electionWinner(votes);
		System.out.println(res);
		in.close();

	}

}
