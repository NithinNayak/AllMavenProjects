package minimummoves;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
public class Solution {
	static int minimumMoves(int[] a, int[] m) {
		int count=0;
		for(int i=0;i<a.length;i++)
		{
			while(a[i]>0&&m[i]>0)
			{
				int a1=a[i]%10;
				int m1=m[i]%10;
				count=count+Math.abs(a1-m1);
				a[i]=a[i]/10;
				m[i]=m[i]/10;
			}
		}
		return count;
	}

}
