package braces;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static String[] braces(String[] values) {
		String res[]=new String[values.length];
		for(int i=0;i<values.length;i++)
		{
			Stack<Character> s=new Stack<Character>();

			for(int j=0;j<values[i].length();j++)
			{
				char ch=values[i].charAt(j);
				if(ch=='{')
				{
					s.push(ch);
				}
				else if(ch=='[')
				{
					s.push(ch);
				}
				else if(ch=='(')
				{
					s.push(ch);
				}
				else if(ch=='}'&&!(s.isEmpty()))
				{
					if(s.peek()=='{')
						s.pop();
				}
				else if(ch==']'&&!(s.isEmpty()))
				{
					if(s.peek()=='[')
						s.pop();	
				}
				else if(ch==')'&&!(s.isEmpty()))
				{
					if(s.peek()=='(')
						s.pop();	
				}

			}
			if(s.isEmpty())
			{
				res[i]="YES";
			}
			else
			{
				res[i]="NO";	
			}


		}
		return res;


	}

}