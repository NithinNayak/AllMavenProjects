package missingwords;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static String[] missingWords(String s, String t) {
		String res="";
		String[] sSplit = s.split(" ");//Splitting the words in a string s
		String[] tSplit = t.split(" ");//Splitting the words in a string t
		for (int sItr = 0; sItr < sSplit.length; sItr++) {
			for (int tItr = 0; tItr < tSplit.length; tItr++) {
				if (sSplit[sItr].equals(tSplit[tItr])) {//Comparing every string in sSplit array with every string in tSplit array
					sSplit[sItr] = "";//If string matches,replace sString by some dummy string like ""
					tSplit[tItr]="";
					break;//Once the match is found,it means that string/word is not missing and hence we need not do further comparisons and hence break the loop
				}
			}
		}
		//sSplit will be containing missing words along with "".The following code neglects "" and stores other words in res array
		int sItr=0,resItr=0;
		while(sItr<sSplit.length)
		{
			if(sSplit[sItr]!="")
			{
				res=res+sSplit[sItr]+" ";
			}
			++sItr;
		}
		return (res.split(" "));

	}

}
