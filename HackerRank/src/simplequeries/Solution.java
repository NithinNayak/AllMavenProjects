package simplequeries;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static int[] counts(int[] nums, int[] maxes) {
		int arr[] = mergeSort(nums);
		int res[] = new int[maxes.length];
		for (int i = 0; i < maxes.length; i++) {
			res[i] = binarySearch(arr, maxes[i]);
		}
		return res;
	}
	
	public static int[] mergeSort(int[] arr)
	{
		int n=arr.length;
		if(n>1)
		{
			int mid=n/2;
			int[] left=new int[mid];
			int[] right=new int[n-mid];
		for(int i=0;i<mid;i++)
			left[i]=arr[i];
		for(int i=mid;i<n;i++)
			right[i-mid]=arr[i];
	    mergeSort(left);
		mergeSort(right);
		merge(arr,left,right);
		}
		return arr;	
	}
	
	
	public static void merge(int[] arr,int[] left,int[] right)
	{
	int l=left.length;
	int r=right.length;
	int i=0,j=0,k=0;
	while(i<l&&j<r)
	{
		if(left[i]<=right[j])
		{
			arr[k]=left[i];
			++i;
		}
		else
		{
			arr[k]=right[j];
			++j;
		}
		++k;
	}
	while(i<l)
	{
		arr[k]=left[i];
		++i;
		++k;
	}
	while(j<r)
	{
		arr[k]=right[j];
		++j;
		k++;
	}
	}
	
	public static int binarySearch(int[] arr,int x)
	{
		int count=0;
		int start=0;
		int end=arr.length-1;
		while(start<=end)
		{
			int mid=(start+end)/2;
			if(arr[mid]<=x)
			{
			++mid;
			while(mid<arr.length&&arr[mid]<=x)
			{
				++mid;
			}
			return mid;
			}
			else if(x<arr[mid])
				end=mid-1;
			else
				start=mid+1;
		}
		return 0;
	}

	

}
