package triangleornot;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    /*
     * Complete the function below.
     */
    static String[] triangleOrNot(int[] a, int[] b, int[] c) {
        String res[]=new String[a.length];
        int min=0,cmp=0;
        for(int i=0;i<a.length;i++)
        {
            if(a[i]<=b[i]&&a[i]<=c[i])
            {
              min=a[i];
                cmp=Math.abs(b[i]-c[i]);
            }
            else if(b[i]<=a[i]&&b[i]<=c[i])
            {
               min=b[i];
                cmp=Math.abs(a[i]-c[i]); 
            }
            else
            {
              min=c[i];
                cmp=Math.abs(b[i]-a[i]);  
            }
            if(min<=cmp)
                res[i]="No";
            else
                res[i]="Yes";
        }
        return res;


	

}
}
