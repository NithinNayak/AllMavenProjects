
package psychometrictesting;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;


public class Solution {
	static int[] jobOffers(int[] scores, int[] lowerLimits, int[] upperLimits) {
		int cnt[]=new int[lowerLimits.length];
		
		/*long startTime=System.currentTimeMillis();*/
		for(int i=0;i<lowerLimits.length;i++)
		{
			for(int j=0;j<scores.length;j++)
				if(scores[j]>=lowerLimits[i]&&scores[j]<=upperLimits[i])
					++cnt[i];
		}
		
		/*long endTime=System.currentTimeMillis();
		System.out.println(endTime-startTime);*/
		return cnt;

	}
}
