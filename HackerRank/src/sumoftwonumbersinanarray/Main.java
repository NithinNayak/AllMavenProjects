package sumoftwonumbersinanarray;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		int n=scan.nextInt();
		int[] a=new int[n];
		for(int i=0;i<n;i++)
		{
			a[i]=scan.nextInt();
		}
		int k=scan.nextInt();
		int res = Solution.numberOfPairs(a, k);
		System.out.println(res);

	}

}
