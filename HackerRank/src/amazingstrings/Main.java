package amazingstrings;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		String[] words = new String[n];
		for (int i = 0; i < n; i++)
			words[i] = scan.next();
		int[] res = Solution.minimalOperations(words);
		for (int x : res) {
			System.out.println(x);
		}

	}

}
