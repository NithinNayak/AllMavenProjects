package amazingstrings;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static int[] minimalOperations(String[] words) {
		int count[] = new int[words.length];//Array which storesminimal operations
		for (int wrdsItr = 0; wrdsItr < words.length; wrdsItr++) {
			char tmp[] = words[wrdsItr].toCharArray();//Converting string into array
			for (int tmpItr = 0; tmpItr < tmp.length - 1; tmpItr++) {
				if (tmp[tmpItr] == tmp[tmpItr+1]) {//It checks every character with its preceding character
					++count[wrdsItr];
					tmp[tmpItr + 1] = '_';//if we find a repeating character,replace it by some character like '_'
				}
			}
		}
		return count;

	}

}
