package balancedornot;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

import java.util.Stack;

public class Solution {
	static int[] balancedOrNot(String[] expressions, int[] maxReplacements) {
		int[] res=new int[maxReplacements.length];
		for(int i=0;i<expressions.length;i++)
		{
			int count=0;
			Stack<Character> stk=new Stack<Character>();
			for(int j=0;j<expressions[i].length();j++)
			{
				if(expressions[i].charAt(j)=='<')
				{
					stk.push(expressions[i].charAt(j));
				}
				if(expressions[i].charAt(j)=='>')
				{
					if(stk.isEmpty())
						++count;
					else
					stk.pop();	
				}
			}
			
			if(count+stk.size()>maxReplacements[i])
				res[i]=0;
			else
				res[i]=1;
		}
		return res;
		
		}

}
