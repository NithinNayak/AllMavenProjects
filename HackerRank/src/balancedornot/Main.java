package balancedornot;

import java.util.Scanner;

public class Main {
	public static void main(String[] args)
	{
   Scanner scan=new Scanner(System.in);
   int x=scan.nextInt();
   String expressions[]=new String[x];
   for(int i=0;i<x;i++)
	   expressions[i]=scan.next();
   x=scan.nextInt();
   int maxReplacements[]=new int[x]; 
   for(int i=0;i<x;i++)
	   maxReplacements[i]=scan.nextInt();
       
   
   int[] res = Solution.balancedOrNot(expressions, maxReplacements);
   for(int y:res)
   System.out.println(y);
}
}
