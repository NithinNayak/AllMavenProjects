package pangrams;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan=new Scanner(System.in);
		int n=scan.nextInt();
		String[] strings=new String[n];
		scan.nextLine();
		for(int i=0;i<n;i++)
			strings[i]=scan.nextLine();
		String res = Solution.isPangram(strings);
		System.out.println(res);

	}

}
