package pangrams;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
public class Solution {
	
	static String isPangram(String[] strings) {
		String res="";
		for(int i=0;i<strings.length;i++)
		{
			int count=0;
			strings[i]=strings[i].replaceAll(" ","");
			for(int j=0;j<strings[i].length();j++)
			{
				if(strings[i].charAt(j)>='a'&&strings[i].charAt(j)<='z')
				{
					++count;
					strings[i]=strings[i].replace(strings[i].charAt(j), '_');
				}
			}
			if(count==26)
			res=res+1;
			else
				res=res+0;
		}
		return res;
}
}
