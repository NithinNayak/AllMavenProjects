package numbercomplement;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static int getIntegerComplement(int n) {
		String binStr ="";
		String cmpStr="";
		double res = 0;
		while (n != 0) {
			int rem = n % 2;
			binStr = binStr + rem;
			n = n / 2;
		}
		int binStrItr=0;
		while (binStrItr < binStr.length()) {
			if (binStr.charAt(binStrItr) == '0')
				cmpStr=cmpStr+'1';
			else
				cmpStr=cmpStr+'0';
			binStrItr++;
		}
		int cmpStrItr=0;
		while (cmpStrItr < cmpStr.length()) {
		int temp=Integer.parseInt(cmpStr.charAt(cmpStrItr)+"");
		double pow=Math.pow(2,cmpStrItr);
		res=res+(temp*pow);
			++cmpStrItr;
		}
		return (int)res;

	}

}
