package theperfectteam;

public class Solution {
	static int differentTeams(String skills) {
		int sklItr = 0, count = 0,min=0;;//count will store number of occurances of skills(like p,c,m,b,z)
		while (sklItr < skills.length()) {//Traversing the skills string
			if (skills.charAt(sklItr) == 'p')//checking for 'p' in skills string
				++count;//if 'p' is found,increase count
			++sklItr;
		}
		min = count;//Initially storing the length for further comparisons
		sklItr = count = 0;
		while (sklItr < skills.length()) {
			if (skills.charAt(sklItr) == 'c')//checking for 'c' in skills string
				++count;//if 'c' is found,increase count
			++sklItr;
		}
		if (count < min)
			min = count;
		sklItr = count = 0;
		while (sklItr < skills.length()) {
			if (skills.charAt(sklItr) == 'm')//checking for 'm' in skills string
				++count;//if 'm' is found,increase count
			++sklItr;
		}
		if (count < min)
			min = count;
		sklItr = count = 0;
		while (sklItr < skills.length()) {
			if (skills.charAt(sklItr) == 'b')//checking for 'b' in skills string
				++count;//if 'b' is found,increase count
			++sklItr;
		}
		if (count < min)
			min = count;
		sklItr = count = 0;
		while (sklItr < skills.length()) {
			if (skills.charAt(sklItr) == 'z')//checking for 'z' in skills string
				++count;//if 'z' is found,increase count
			++sklItr;
		}
		if (count < min)
			min = count;
		return min;

	}

}
