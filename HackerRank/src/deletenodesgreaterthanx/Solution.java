package deletenodesgreaterthanx;

import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {
	public static class LinkedListNode {
		int val;
		LinkedListNode next;

		LinkedListNode(int node_value) {
			val = node_value;
			next = null;
		}
	};

	public static LinkedListNode _insert_node_into_singlylinkedlist(LinkedListNode head, LinkedListNode tail, int val) {
		if (head == null) {
			head = new LinkedListNode(val);
			tail = head;
		} else {
			tail.next = new LinkedListNode(val);
			tail = tail.next;
		}

		return tail;
	}

	static LinkedListNode removeNodes(LinkedListNode listHead, int x) {
		while(listHead.val>x)
		{
			listHead=listHead.next;
		}
		 LinkedListNode prevNode = null;
			LinkedListNode currentNode = listHead;
			while (currentNode != null) {	
				if (currentNode.val > x) {
					prevNode.next = currentNode.next;
					currentNode = null;
					currentNode = prevNode.next;
				} else {
					prevNode = currentNode;
					currentNode = currentNode.next;
				}
			}
			return listHead;

	}

}
