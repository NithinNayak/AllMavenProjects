package deletenodesgreaterthanx;

import java.util.Scanner;

import deletenodesgreaterthanx.Solution.LinkedListNode;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		int N=scan.nextInt();
		LinkedListNode listHead=null;
		for(int i=0;i<N;i++)
		{
			if(listHead==null)
			{
				listHead=new LinkedListNode(scan.nextInt());
			}
			else
			{
				LinkedListNode listTail=listHead;
				while(listTail.next!=null)
				{
					listTail=listTail.next;
				}
				listTail.next=new LinkedListNode(scan.nextInt());
			}
		}
		int x=scan.nextInt();
		
        LinkedListNode res =Solution.removeNodes(listHead, x);

        while (res != null) {
        	System.out.println(res.val);
        	res=res.next;
        }

        
	}

}
