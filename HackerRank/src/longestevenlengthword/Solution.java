package longestevenlengthword;
public class Solution {
	static String longestEvenWord(String sentence) {
		String splitStr[] = sentence.split(" ");//Cut the string at whitespaces
		String res = quickSort(splitStr, 0, splitStr.length - 1);//Sort the words according to length and return longest even length word
		return res;

	}

	public static String quickSort(String arr[], int start, int end) {//Sorting the words ain the increasing order of their lengths
		if (start < end) {
			int pIndex = partition(arr, start, end);
			quickSort(arr, start, pIndex - 1);
			quickSort(arr, pIndex + 1, end);
		}
		if(arr[0].length()%2==0)//If there is only one string and if its length is even,return that string
		return arr[0];
		else
			return "00";//else return 00
	}

	public static int partition(String arr[], int start, int end) {
		String pivot = arr[end];
		int pIndex = start;
		for (int i = start; i < end; i++) {
			if (arr[i].length() >= pivot.length() && (arr[i].length()) % 2 == 0) {//Identifying longest even length words and arranging them
				String temp = arr[pIndex];
				arr[pIndex] = arr[i];
				arr[i] = temp;
				++pIndex;
			}
		}
		String temp = arr[pIndex];
		arr[pIndex] = pivot;
		arr[end] = temp;
		return pIndex;
	}

}
