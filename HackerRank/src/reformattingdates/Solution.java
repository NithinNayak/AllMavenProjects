package reformattingdates;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static String[] reformatDate(String[] dates) {
		String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };//It ius used to compare the month
		String[] res = new String[dates.length];//Stores the result of reformatted date
		for (int dateItr = 0; dateItr < dates.length; dateItr++) {//Comparing each date string present in dates array in each iteration
			if (dates[dateItr].charAt(0) <= '3' && dates[dateItr].charAt(1) <= '9') {//This is to confirm whether the date is a two digit
				res[dateItr] = dates[dateItr].substring(9, 13);//Year is stored initially
				for (int mnthItr = 0; mnthItr < 12; mnthItr++) {//Used to get the number of the month
					if ((dates[dateItr].substring(5, 8)).equals(months[mnthItr])) {//Comparing the month in dates[] with the months present in months array
						++mnthItr;
						if (mnthItr <= 9) {//if month is less than 10,then append 0 else don't append
							res[dateItr] = res[dateItr] + '-' + '0' + mnthItr + '-' + dates[dateItr].substring(0, 2);
						} else {
							res[dateItr] = res[dateItr] + '-' + mnthItr + '-' + dates[dateItr].substring(0, 2);
						}
						break;
					}
				}
			} else {//this is executed when date is a single digit
				res[dateItr] = dates[dateItr].substring(8, 12);//Storing the year
				for (int mnthItr = 0; mnthItr < 12; mnthItr++) {
					if ((dates[dateItr].substring(4, 7)).equals(months[mnthItr])) {//Getting month number
						++mnthItr;
						if (mnthItr <= 9) {
							res[dateItr] = res[dateItr] + '-' + '0' + mnthItr + '-' + '0'
									+ dates[dateItr].substring(0, 1);
						} else {
							res[dateItr] = res[dateItr] + '-' + mnthItr + '-' + '0' + dates[dateItr].substring(0, 1);
						}
						break;
					}
				}
			}
		}

		return res;

	}

}
