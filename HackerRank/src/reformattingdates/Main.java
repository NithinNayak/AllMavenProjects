package reformattingdates;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		int n=scan.nextInt();
		String[] dates=new String[n];
		scan.nextLine();
		for(int i=0;i<n;i++)
			dates[i]=scan.nextLine();
		 String[] res = Solution.reformatDate(dates);
		 for(String x:res)
		 {
			 System.out.println(x);
		 }

	}

}
