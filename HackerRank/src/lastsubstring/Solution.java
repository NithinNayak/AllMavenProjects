package lastsubstring;

public class Solution {
	static String compute(String s) {
		int minIndex=0;//Used to store the starting/minimum index of last substring
		char ch=s.charAt(0);//Used to store the greater character present in string s
		for(int sItr=0;sItr<s.length();sItr++)
		{
			if(s.charAt(sItr)>ch)//If any character is greater than another,it means that substring starts from there
			{
				ch=s.charAt(sItr);//Storing the greatest character for next iteration
				minIndex=sItr;//Stores the minimum index of highest character
			}
		}
		return(s.substring(minIndex));

    }

}
