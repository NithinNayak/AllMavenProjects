package consecutivesum;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
public class Solution {
	
	static int consecutive(long num) {
		int start=1;
		long end=0;
		int count=0;
		if(num%2==0)
		end=num/2;
		else
		end=num/2+1;	
		while(start<end)
		{
			int sum=0;
			for(int i=start;i<=end;i++)
			{
				sum=sum+i;
				if(sum==num){
					++count;
				break;}
			}
			++start;
		}
return count;
	}
	

}
