package redundancyinalinkedlist;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

class LinkedListNode {
	int data;
	LinkedListNode next;

	LinkedListNode(int val) {
		data = val;
		next = null;
	}

}

public class Solution {
	public static LinkedListNode insert(LinkedListNode head, int val) {
		if (head == null) {
			head = new LinkedListNode(val);
		} else {
			head.next = insert(head.next, val);
		}
		return head;
	}

	static LinkedListNode distinct(LinkedListNode head) {
		LinkedListNode exHead = head;// Taking a reference to head of list
		while (exHead.next != null)// Traverse till last but one node
		{
			LinkedListNode currentHead = exHead.next;// Nodes to be compared
														// exHead's data
			LinkedListNode headFollower = exHead;// It follows currentHead
			while (currentHead != null)// Traverse till the end node
			{
				if (currentHead.data == exHead.data) {
					headFollower.next = currentHead.next;
					currentHead = null;
					currentHead = headFollower.next;
				} else {
					headFollower = currentHead;
					currentHead = currentHead.next;
				}
			}
			if(exHead.next!=null)
			exHead = exHead.next;
		}
		return head;

	}

}
