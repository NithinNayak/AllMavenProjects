package redundancyinalinkedlist;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		LinkedListNode head = null;
		LinkedListNode tail = null;
		int n = scan.nextInt();
		for (int i = 0; i < n; i++) {
			head = Solution.insert(head, scan.nextInt());
		}
		LinkedListNode headd = head;
		while (headd != null) {
			System.out.print(headd.data + " ");
			headd = headd.next;
		}
		System.out.println();
		LinkedListNode res = Solution.distinct(head);
		while (res != null) {
			System.out.print(res.data + " ");
			res = res.next;
		}

	}

}
