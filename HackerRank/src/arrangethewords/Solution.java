package arrangethewords;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static String arrange(String sentence) {
		int n = 0;
		StringBuffer resStr = new StringBuffer();
		sentence = sentence.toLowerCase();
		sentence = sentence.replace('.',' ' );
		StringTokenizer stk1 = new StringTokenizer(sentence, " ");
		while (stk1.hasMoreTokens()) {
			stk1.nextToken();
			++n;
		}
		String resArr[] = new String[n];
		StringTokenizer stk2 = new StringTokenizer(sentence, " ");
		int i = 0;
		while (stk2.hasMoreTokens()) {
			resArr[i] = stk2.nextToken();
			++i;
		}
		for (int j = 0; j< i - 1; j++) {
			for (int k = 0; k < i - 1 - j; k++) {
				if (resArr[k].length() > resArr[k + 1].length()) {
					String tempString = resArr[k];
					resArr[k] = resArr[k + 1];
					resArr[k + 1] = tempString;
				}
			}
		}
		for (int l = 0; l < resArr.length; l++) {
			resStr.append(resArr[l] + " ");
		}
		resStr.setCharAt(resStr.length() - 1, '.');
		resStr.setCharAt(0, (char) (resStr.charAt(0)-32));
		return new String(resStr);

	}

}
