package modifyprices;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static int verifyItems(String[] origItems, float[] origPrices, String[] items, float[] prices) 
	{
		int cnt=0;
		for(int i=0;i<items.length;i++)
		{
			for(int j=0;j<origItems.length;j++)
			{
				if(items[i].equals(origItems[j]))
				{
					if(prices[i]!=origPrices[j])
					{
						++cnt;
					}
				}
			}
		}
		return cnt;
	}


}
