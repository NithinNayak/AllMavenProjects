package modifyprices;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int i=in.nextInt();
		int j=0;
		String origItems[]=new String[i];
		for(j=0;j<i;j++)
		{
		origItems[j]=in.next();
		}
		i=in.nextInt();
		float origPrices[]=new float[i];
		for(j=0;j<i;j++)
		{
		origPrices[j]=in.nextFloat();
		}
		i=in.nextInt();
		String items[]=new String[i];
		for(j=0;j<i;j++)
		{
		items[j]=in.next();
		}
		i=in.nextInt();
		float prices[]=new float[i];
		for(j=0;j<i;j++)
		{
		prices[j]=in.nextFloat();
		}
		
        int res = Solution.verifyItems(origItems, origPrices, items, prices);
        System.out.println(res);

	}

}
