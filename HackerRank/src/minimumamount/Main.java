package minimumamount;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		int[] prices = new int[n];
		for (int i = 0; i < n; i++)
			prices[i] = scan.nextInt();
		long res = Solution.calculateAmount(prices);
		System.out.println(res);

	}

}
