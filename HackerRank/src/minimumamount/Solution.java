package minimumamount;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
public class Solution {
	static long calculateAmount(int[] prices) {
		int discount=prices[0];//To store minimum discount
		long totalAmount=prices[0];//First item is compulsory and no discount
		for(int prcItr=1;prcItr<prices.length;prcItr++)
		{
			if(discount>prices[prcItr])
			{
				discount=prices[prcItr];//If discount is greater than current item price,then item is free of cost and minimum discount is price of current item
			}
			else
			{
				totalAmount+=prices[prcItr]-discount;//Else amount will be decreased by discount and added to totalAmount
			}
		}
		return totalAmount;
   }

}
