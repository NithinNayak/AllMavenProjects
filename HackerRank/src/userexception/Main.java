package userexception;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		try {
			int num1 = scan.nextInt();
			int num2 = scan.nextInt();
			if (num2 == 0)
				throw new MyException();
			int res = num1 / num2;
			System.out.println(res);
		} catch (MyException e) {
			System.out.println(e.getMessage());
		}

	}

}
