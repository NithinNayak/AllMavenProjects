package reachingpoints;

public class Solution {
	static String canReach(int x1, int y1, int x2, int y2) {
		String temp1 = "", temp2 = "";
		if (x1 == x2 && y1 == y2)
			return "YES";
		if (x1 > x2||y1>y2)
			return "NO";
		temp1 = canReach(x1 + y1, y1, x2, y2);
		temp2 = canReach(x1, x1 + y1, x2, y2);
		if (temp1.equals("YES") || temp2.equals("YES"))
			return "YES";
			return "NO";

	}

}
