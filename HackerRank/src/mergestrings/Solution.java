package mergestrings;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	static String mergeStrings(String a, String b) {
        String mergedString="";
        int i=0;
        while(i!=a.length()&&i!=b.length())
        {
            mergedString=mergedString+a.charAt(i)+b.charAt(i);
            ++i;
        }
        if(a.length()>b.length())
        {
            while(i!=a.length())
            {
              mergedString=mergedString+a.charAt(i); 
                ++i;
            }
        }
        else
        {
            while(i!=b.length())
            {
              mergedString=mergedString+b.charAt(i);  
                ++i;
            }
        }
        return mergedString;

}
}
