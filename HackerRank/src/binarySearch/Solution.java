package binarySearch;

public class Solution {
	public static int binarySearch(int[] arr,int start,int end,int x)
	{
		if(start>end)
			return -1;
		int mid=(start+end)/2;
		if(arr[mid]==x)
		{
			int ans=mid;
			return ans;
		}
		if(x<arr[mid])
			return binarySearch(arr,start,mid-1,x);
		else
			return binarySearch(arr,mid+1,end,x);
	}
}
