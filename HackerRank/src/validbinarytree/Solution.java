package validbinarytree;


public class Solution {
	public static class Node {
		Node left, right;
		int data;

		Node(int newData) {
			left = right = null;
			data = newData;
		}
	};
	public static Node insert(Node node, int data) {
		if (node==null) {
			node = new Node(data);
		}
		else {
			if (data <= node.data) {
				node.left = insert(node.left, data);
			}
			else {
				node.right = insert(node.right, data);
			}
		}
		return(node);
	}
	public static int isPresent(Node root, int val)
	{
		if(root==null)
		{
			return 0;
		}
		if(root.data==val)
		{
			return 1;
		}
			if(val<root.data)
			{
				return isPresent(root.left,val);
			}
			else
			
			{
				return isPresent(root.right,val);
			}
	}
	

}
