package com.mindtree.client;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.mindtree.entity.Department;
import com.mindtree.entity.Employee;

public class MainUser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Configuration config=new Configuration();
		config.configure();
		SessionFactory sessionFactory=config.buildSessionFactory();
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		Employee e1=new Employee();
		Department d=new Department();
		e1.setEmployeeName("X");
		e1.setEmail("nayakni@gmail.com");
		e1.setSalary(789.00);
		e1.setDateOfBirth(new java.util.Date());
		Employee e2=new Employee();
		e2.setEmployeeName("Y");
		e2.setEmail("nayakni@mail.com");
		e2.setSalary(78.00);
		e2.setDateOfBirth(new java.util.Date());
		 ArrayList<Employee> list1=new ArrayList<Employee>();
		 list1.add(e1);
		 list1.add(e2);
		 d.setDepartmentname("Shaaaaa");
		 d.setEmployee(list1);
		 session.save(d);
		 session.beginTransaction().commit();
		 session.close();
		 

	}

}
