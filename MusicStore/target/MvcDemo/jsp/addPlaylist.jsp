<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script>
function checkFields()
{
var playListName=document.getElementById("playListName").value;
if(playListName=="")
	{
	alert("Playlist field can't be empty");
	return false;
	}
else
	{
for (var c in document.getElementsByClassName("cb")) {
	if(c.checked)
		{
		return true;
		}
	else
		{
	alert("Please choose a song");
	return false;
	}
}
}
</script>
<style>
div
{
height: 700px;
	width: 1000px;
	margin-left: 20%;
	background-color: red;
	border-radius: 40%;
	background-repeat: no-repeat;
	background-size: 100% 100%;
	font-size:180%;
}
</style>
</head>
<body>
<form action="addPlaylist" method="post">
<div align=center>
</br></br></br></br>
<table>

<c:forEach items="${songList}" var="item">
<tr>
<td>
    <input type="checkbox" name="songNames" class="cb" value="${item.getSongName()}"/>${item.getSongName()}
    </td>
    </tr>
</c:forEach>
<tr><td></td></tr>
<tr>
<td>Enter Playlist Name</td>
<td><input type="text" name="playListName" id="playListName"/></td>
</tr>
<tr>
<td></td>
<td>
<input type="submit" onclick="return checkFields()" name="press" value="add"/>
<input type="submit" onclick="return checkPlayListField()" name="press" value="search"/>
<input type="submit" name="press" value="cancel"/>
</td>
</tr>
</table>
${msg}
<c:forEach items="${songs}" var="item">
    ${item}</br>
</c:forEach>
</div>
</form>
</body>
</html>