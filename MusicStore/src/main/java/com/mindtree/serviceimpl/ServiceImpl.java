package com.mindtree.serviceimpl;
import java.util.List;

import com.mindtree.dao.Dao;
import com.mindtree.daoimpl.DaoImpl;
import com.mindtree.entity.MusicDetails;
import com.mindtree.entity.PlayListDetails;
import com.mindtree.service.Service;

public class ServiceImpl implements Service{
	public boolean addSongs(String songName,String artist,String location,String album,int duration,String tags,Dao dao)
	{
		MusicDetails musicDetails=new MusicDetails();
		musicDetails.setSongName(songName);
		musicDetails.setArtist(artist);
		musicDetails.setLocation(location);
		musicDetails.setAlbum(album);
		musicDetails.setDuration(duration);
		musicDetails.setTags(tags);
		return(dao.addSongs(musicDetails));
	}
	public List<MusicDetails> fetchSongs(Dao dao)
	{
		List<MusicDetails> songList=dao.fetchSongs();
		return songList;
	}
	public boolean addPlaylist(Dao dao,String playlistname,String[] songNames)
	{
		PlayListDetails playListDetails=new PlayListDetails();
		playListDetails.setPlayListName(playlistname);
		boolean isPresent=dao.addPlaylists(playListDetails,songNames);
		return isPresent;
	}
	public List<String> searchPlaylist(Dao dao,String playListName)
	{
		List<String> songList=dao.searchPlaylist(playListName);
		return songList;
	}
}
