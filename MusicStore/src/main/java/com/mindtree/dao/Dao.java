package com.mindtree.dao;

import java.util.List;

import com.mindtree.entity.MusicDetails;
import com.mindtree.entity.PlayListDetails;

public interface Dao {
	boolean addSongs(MusicDetails musicDetails);
	List<MusicDetails> fetchSongs();
	boolean addPlaylists(PlayListDetails playListDetails,String[] songNames);
	List<String> searchPlaylist(String playListName);

}
