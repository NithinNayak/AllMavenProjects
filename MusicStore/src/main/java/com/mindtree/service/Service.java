package com.mindtree.service;

import java.util.List;

import com.mindtree.dao.Dao;
import com.mindtree.entity.MusicDetails;

public interface Service {
	boolean addSongs(String songName,String artist,String location,String album,int duration,String tags,Dao dao);
	List<MusicDetails> fetchSongs(Dao dao);
	boolean addPlaylist(Dao dao,String playlistname,String[] songNames);
	List<String> searchPlaylist(Dao dao,String playListName);
}
