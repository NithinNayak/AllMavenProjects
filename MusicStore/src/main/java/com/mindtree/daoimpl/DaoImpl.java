package com.mindtree.daoimpl;

import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindtree.dao.Dao;
import com.mindtree.entity.MusicDetails;
import com.mindtree.entity.PlayListDetails;

@Component
public class DaoImpl implements Dao{
	@Autowired
	private SessionFactory sessionFactory;
	@Transactional
    public boolean addSongs(MusicDetails musicDetails){  
          Session session=sessionFactory.getCurrentSession();
          session.save(musicDetails);
          System.out.println("Added successfully");
          return true;
    }
	@Transactional
	public List<MusicDetails> fetchSongs()
	{
		Session session=sessionFactory.getCurrentSession();
		Query query=session.createQuery("from MusicDetails");
		List<MusicDetails> res=query.list();
		return res;
	}
	@Transactional
	public boolean addPlaylists(PlayListDetails playListDetails,String[] songNames)
	{
		Session session=sessionFactory.getCurrentSession();
		Query query=session.createQuery("from PlayListDetails");
		List<PlayListDetails> res=query.list();
		Iterator<PlayListDetails> itr=res.iterator();
		while(itr.hasNext())
		{
			String playListName=itr.next().getPlayListName();
			if((playListDetails.getPlayListName()).equals(playListName))
			{
				return true;
			}
		}
		MusicDetails musicDetails=null;
		System.out.println(songNames);
		for(String song:songNames)
		{
			System.out.println(song);
			musicDetails=((MusicDetails)(session.get(MusicDetails.class,song)));
			musicDetails.getPlayListDetails().add(playListDetails);
			playListDetails.getMusicDetails().add(musicDetails);
		}
		session.update(musicDetails);
		session.save(playListDetails);
		return false;
	}
	@Transactional
	public List<String> searchPlaylist(String playListName)
	{
		boolean isPresent=false;
		List<String> songList=null;
		
		Session session=sessionFactory.getCurrentSession();
		Query query=session.createQuery("from PlayListDetails");
				List<PlayListDetails> res=query.list();
				Iterator<PlayListDetails> itr=res.iterator();
				while(itr.hasNext())
				{
					if(playListName.equals(itr.next().getPlayListName()))
						isPresent=true;
				}
				if(isPresent)
				{
					query=session.createQuery("select m.songName from MusicDetails m join m.playListDetails as p where p.playListName='" + playListName + "'");
					songList=query.list();
					Iterator<String> itr1=songList.iterator();
					while(itr1.hasNext())
					{
						System.out.println((itr1.next()));
					}
					return songList;	
				}
				else
				{
					return songList;
				}
				
	}
	

}
