package com.mindtree.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mindtree.dao.Dao;
import com.mindtree.entity.MusicDetails;
import com.mindtree.serviceimpl.ServiceImpl;

@Controller
public class MusicControl {
	@Autowired
	Dao dao;
	ServiceImpl service = new ServiceImpl();

	@RequestMapping(value = "/addSongs")
	//It handles the "Add Songs" link and returns "addSongs" view 
	public String addSong() {
		return "addSongs";
	}

	@RequestMapping(value = "/addPlaylist") 
	//It handles "Add/Search playlist" link and returns a "addPlaylist" view with the model of song lists
	public ModelAndView addPlayList() {
		List<MusicDetails> songLists = service.fetchSongs(dao);
		ModelAndView model = new ModelAndView("addPlaylist");
		model.addObject("songList",songLists);
		return model;
	}

	@RequestMapping(value = "/addSongs", method = RequestMethod.POST)
	//This handles the POST request from addSongs page
	public ModelAndView addSongs(HttpServletRequest req, HttpServletResponse res) {
		String songName = null, artist = null, location = null, album = null, tags = null;
		int duration = 0;
		//If in case,user clicks on cancel button,homepage view will be returned
		if ((req.getParameter("press")).equals("cancel")) {
			ModelAndView model = new ModelAndView("homepage");
			return model;
		}
		//If user clicks on add button,then values from all the fields will be fetched and stored in variables as shown below
		songName = req.getParameter("songName");
		artist = req.getParameter("artist");
		location = req.getParameter("location");
		album = req.getParameter("album");
		duration = Integer.parseInt(req.getParameter("duration"));
		tags = req.getParameter("tags");
		//These details will be passed to service layer method,which does the rest of the work
		service.addSongs(songName, artist, location, album, duration, tags, dao);
		//After successfull addition of song,user will be redirected to homepage
		ModelAndView model = new ModelAndView("homepage");
		model.addObject("msg", "Song added successfully");
		return model;
	}
	//This handles the POST request from addPlaylist page
	@RequestMapping(value = "/addPlaylist", method = RequestMethod.POST)
	public ModelAndView addPlaylist(HttpServletRequest req, HttpServletResponse res) {
		//If user presses on cancel button,user will be redirected to homepage
		if ((req.getParameter("press")).equals("cancel")) {
			ModelAndView model = new ModelAndView("homepage");
			return model;
		}
		//If presses clicks on search button,this condition becomes true
		else if ((req.getParameter("press")).equals("search")) {
			String searchListName = req.getParameter("playListName");
			//Service layer method will be called to search for the playlist which will return the songlist present in that playlist
			List<String> songList = service.searchPlaylist(dao, searchListName);
			ModelAndView model = new ModelAndView("addPlaylist");
			//If the playlist doesn't exist,a error message will be displayed on "addPlaylist" page
			if (songList == null) {
				model.addObject("msg", "No playlist found");
				return model;
			}//Or else return the songs present in the playlist to "addPlaylist" page 
			else {
				model.addObject("songs", songList);
				return model;
			}
		}
		//
		else {
			List<MusicDetails> songList = service.fetchSongs(dao);
			ModelAndView model = new ModelAndView("addPlaylist");
			model.addObject("songList", songList);
			String[] songNames = req.getParameterValues("songNames");

			String playlistname = req.getParameter("playListName");
			boolean isPresent = service.addPlaylist(dao, playlistname, songNames);
			if (isPresent) {
				model.addObject("msg", "PlayList Already Present");
			} else {
				model.addObject("msg", "Playlist Successfully added");
			}
			return model;
		}
	}

}