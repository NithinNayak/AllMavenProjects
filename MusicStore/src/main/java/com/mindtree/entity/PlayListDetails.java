package com.mindtree.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class PlayListDetails {
	@Id
private String playListName;
	@ManyToMany
private List<MusicDetails> musicDetails=new ArrayList<MusicDetails>();
	
	public List<MusicDetails> getMusicDetails() {
		return musicDetails;
	}

	public void setMusicDetails(List<MusicDetails> musicDetails) {
		this.musicDetails = musicDetails;
	}

	public String getPlayListName() {
		return playListName;
	}

	public void setPlayListName(String playListName) {
		this.playListName = playListName;
	}


	
}
