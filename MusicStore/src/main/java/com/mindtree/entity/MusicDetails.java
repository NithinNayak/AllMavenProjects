package com.mindtree.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class MusicDetails {
	@Id
	private String songName;
	private String artist;
	private String location;
	private String album;
	private int duration;
	private String tags;
	@ManyToMany(mappedBy="musicDetails")
	private List<PlayListDetails> playListDetails=new ArrayList<PlayListDetails>();
	
	public List<PlayListDetails> getPlayListDetails() {
		return playListDetails;
	}
	public void setPlayListDetails(List<PlayListDetails> playListDetails) {
		this.playListDetails = playListDetails;
	}
	public String getSongName() {
		return songName;
	}
	public void setSongName(String songName) {
		this.songName = songName;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getAlbum() {
		return album;
	}
	public void setAlbum(String album) {
		this.album = album;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	
	

}
