<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script>
	function validator(id) {
		var name = document.getElementById("name").value;
		if (name == "") {
			alert("Name field can't be empty");
			return false;
		}
		var location = document.getElementById("loc").value;
		if (location == "") {
			alert("Location can't be empty");
			return false;
		}
		var n = document.getElementById("dur").value;
		if (isNaN(n)) {
			alert("Duration should be a numeric value");
			return false;
		}
		if (n == "") {
			alert("Duration can't be empty");
			return false;
		}
		return true;
	}
</script>
<style>
div {
	height: 600px;
	width: 600px;
	margin-left: 35%;
	margin-top: 5%;
	background-color: red;
	align-content: center;
	border-radius: 40%;
	background-image:
		url("https://giphy.com/gifs/sherlockgnomes-sherlock-l4pTfx2qLszoacZRS");//this is a gif.So dsnt wrk//
	background-repeat: no-repeat;
	background-size: 100% 100%;
	font-size:150%;
}
</style>
</head>
<body>
	<form action="addSongs" method="post">
	<div align=center>
	</br></br></br></br></br></br>
		<table>
			<tr>
				<td>Song Title/Name</td>
				<td><input type="text" name="songName" id="name" /></td>
			</tr>
			<tr></tr><tr></tr><tr></tr>
			<tr>
				<td>Artist(s)</td>
				<td><input type="text" name="artist" /></td>
			</tr>
			<tr></tr><tr></tr><tr></tr>
			<tr>
				<td>Location</td>
				<td><input type="text" name="location" id="loc" /></td>
			</tr>
			<tr></tr><tr></tr><tr></tr>
			<tr>
				<td>Album</td>
				<td><input type="text" name="album" /></td>
			</tr>
			<tr></tr><tr></tr><tr></tr>
			<tr>
				<td>Duration</td>
				<td><input type="text" id="dur" name="duration" /></td>
			</tr>
			<tr></tr><tr></tr><tr></tr>
			<tr>
				<td>Tags</td>
				<td><input type="text" name="tags" /></td>
			</tr>
			<tr></tr><tr></tr><tr></tr>
			<tr>
				<td></td>
				<td>
				<input type="submit" name="press" value="add"
					onclick="return validator()" />
				<input type="submit" name="press" value="cancel" />
				</td>
			</tr>
		</table>
		</div>
	</form>
</body>
</html>