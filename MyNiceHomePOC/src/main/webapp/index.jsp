<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD></HEAD>
<BODY>
<div align="center" style="margin-top:10%">
${msg}</br></br>
<a href="addProperty" style="text-decoration: none">Click here to add a property</a></br></br>
<a href="deleteProperty" style="text-decoration: none">Click here to delete a property</a></br></br>
<!-- <a href="editProperty" style="text-decoration: none">Click here to edit the property</a></br></br> -->
</div>
</BODY>
</HTML>