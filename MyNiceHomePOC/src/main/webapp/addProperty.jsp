<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!doctype html>
<html>
<head>
<script src="http://code.jquery.com/jquery-1.7.2.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
#div2
{
 background-image:url("http://ramadacochin.com/sites/default/files/Luxury%20Room%20(2).jpg");
 background-repeat: no-repeat;
 background-attachment: fixed;
background-size:100% 100%;
}
#div1
{
background-color: skyblue;
height:100px;
}
</style>
<script type="text/javascript">
function validateFields()
{
	var name=document.getElementById("name").value;
	var address=document.getElementById("address").value;
	var city=document.getElementById("city").value;
		var state=document.getElementById("state").value;
		var rooms=document.getElementById("noofrooms").value;
			var capacity=document.getElementById("capacity").value;
			var images=document.getElementById("image").value;
				var cost=document.getElementById("cost").value;
					var family=document.getElementById("aboutfamily").value;
					var checkName=/^[a-zA-z/s]+$/;
					if(checkName.test(name)==false)
						{
						document.getElementById("namealert").innerHTML="Enter a valid name";
						return false; 
						}
					else if(address=="" ||(!(isNaN(address))))
					{
					document.getElementById("addressalert").innerHTML="Please enter the address";
					return false;
					}
					else if(city=="--City--")
					{
					document.getElementById("cityalert").innerHTML="Select a city";
					return false;
					}
					else if(state=="--State--")
					{
					document.getElementById("statealert").innerHTML="Select a state";
					return false;
					}
					else if(rooms=="")
					{
						document.getElementById("roomsalert").innerHTML="Please select atleast one room";
						return false;					
						}
					else if(capacity=="")
					{
						document.getElementById("capacityalert").innerHTML="Please select atleast one capacity";
						return false;					
						}
					else if(images=="")
					{
						document.getElementById("imagealert").innerHTML="Please upload atleast one picture";
						return false;					
						}
					else if((isNaN(cost))||(cost=="")||(cost<=0)||(cost>5000))
					{
						document.getElementById("costalert").innerHTML="Please enter a valid input";
						return false;
					}
					else if(family=="" ||(!(isNaN(family))))
					{
					document.getElementById("familyalert").innerHTML="Please enter the family information";
					return false;
					}
					
					else
						{
						return true;
						}
						
}
function blank(id)
{
	document.getElementById(id).innerHTML="";
	
	}

function dynamicdropdown()
{
    var listindex=document.getElementById("state").value;
    switch (listindex)
    {
    case "Karnataka" :
        document.getElementById("city").options[0]=new Option("Select City","");
        document.getElementById("city").options[1]=new Option("Bengaluru","bengaluru");
        document.getElementById("city").options[2]=new Option("Mangaluru","mangaluru");
        document.getElementById("city").options[3]=new Option("Mysore","mysore");
        document.getElementById("city").options[4]=new Option("Udupi","udupi");
        break;
    case "Andhra Pradesh" :
        document.getElementById("city").options[0]=new Option("Select City","");
        document.getElementById("city").options[1]=new Option("Vijayawada","vijaya");
        document.getElementById("city").options[2]=new Option("Rajamundri","raja");
        document.getElementById("city").options[3]=new Option("Amaravati","amara");
        document.getElementById("city").options[4]=new Option("Kakinada","kaki");
        break;
        case "Odisha" :
        document.getElementById("city").options[0]=new Option("Select City","");
        document.getElementById("city").options[1]=new Option("Bhubaneswar","bhuba");
        document.getElementById("city").options[2]=new Option("Cuttack","cut");
        document.getElementById("city").options[3]=new Option("Puri","pu");
        document.getElementById("city").options[4]=new Option("Balasore","bala");
        break;
        case "Punjab" :
        document.getElementById("city").options[0]=new Option("Select City","");
        document.getElementById("city").options[1]=new Option("Chandigarh","chandi");
        document.getElementById("city").options[2]=new Option("Ludhiana","ludhi");
        document.getElementById("city").options[3]=new Option("Jalandhar","jala");
        document.getElementById("city").options[4]=new Option("Patiala","pati");
        break;
    }
    return true;
}



</script>
</head>
<body>
<div style="visibility:visible">
<div id="div1">
</br>
<h1 style="margin-top:10px">MyNiceHome <i class="fa fa-home" style="font-size:48px;color:black;"></i><a href="index" style="text-decoration: none;margin-left:1200px">Home</a></h1>
</div>
	<div align="center" id="div2">
		<form action="addProperty" method="post">
			<table  cellpadding="10px">
				<tr>
					<td><label style="font-size: 20px;color:black;font-weight: bold">Name of the property</label></td>
					<td><input type="text" name="propName" id="name" style="border-radius:10px;height:30px;width:250px" onClick="blank('namealert')"><span style="color:red;font-size:20px" id="namealert"></span>
						</td>
				</tr>




				<tr>
					<td><label style="font-size: 20px;color:black;font-weight: bold">Address</label></td>
					<td><textarea name="address" rows="3" cols="33" id="address" style="border-radius:10px" onClick="blank('addressalert')"></textarea><span style="color: red;font-size:20px" id="addressalert"></span></td>
				</tr>

				<tr>
					<td></td>
					<td><select name="state" id="state" onClick="blank('statealert')" onchange="dynamicdropdown()">
				<option disabled selected>--State--</option>
        		<option>Karnataka</option>
        		<option>Andhra Pradesh</option>
        		<option>Odisha</option>
        		<option>Punjab</option>
        		</select>
					
					<script type="text/javascript" language="JavaScript">
        document.write('<select name="city" id="city"><option>--City--</option></select>')
        </script>
					
					<span style="color: red;font-size:20px" id="statealert"></span> <span style="color:red;font-size:20px" id="cityalert"></span></td>
				</tr>
				<tr>



					<td><label style="font-size: 20px;color:black;font-weight: bold">Number of rooms</label></td>
					<td><input type="number" name="noofrooms" max=10 min=1 style="width: 50px;border-radius:10px" id="noofrooms" onClick="blank('roomsalert')"><span style="color:red;font-size:20px" id="roomsalert"></span>
					</td>
				</tr>
				<tr>

					<td><label style="font-size: 20px;color:black;font-weight: bold">Capacity</label></td>
					<td><input type="number" max=10 min=1 name="capacity" style="width:50px;border-radius:10px" id="capacity" onClick="blank('capacityalert')"><span style="color:red;font-size:20px" id="capacityalert"></span>
					</td>
				</tr>
				<tr>
					<td><label style="font-size: 20px;color:black;font-weight: bold">Facilities</label></td>

					<td><input type="checkbox" value="Wi-Fi" name="facilities"><span style="color: black">Wi-Fi</span>
						<input type="checkbox" value="Food" name="facilities" style="margin-left:33px"><span style="color: black">Pool</span></br>
						<input type="checkbox" value="Swimming Pool" name="facilities"><span style="color: black">Food</span>
						<input type="checkbox" value="Dj" name="facilities" style="margin-left:38px"><span style="color: black">Dj</span>
					</td>
				</tr>

				<tr>
					<td><label style="font-size: 20px;color:black;font-weight: bold">Upload images</label></td>
					<td><input type="file" multiple name="images" accept="image/*" id="image" onClick="blank('imagealert')"><span style="color: red;font-size:20px" id="imagealert"></span>
					</td>
				</tr>

				<tr>
					<td><label style="font-size: 20px;color:black;font-weight: bold">Cost</label></td>
					<td><input type="text" name="cost" style="width:50px;border-radius:10px" id="cost" onClick="blank('costalert')"><span style="color: red;font-size:20px" id="costalert"></span></td>
				</tr>

				<tr>
					<td><label style="font-size: 20px;color:black;font-weight: bold">About Family</label></td>
					<td><textarea name="aboutfamily" rows="3" cols="33" id="aboutfamily" style="border-radius:10px" onClick="blank('familyalert')"></textarea><span style="color: red;font-size:20px" id="familyalert"></span></td>

				</tr>

				<tr>
					<td></td>
					<td><input type="submit" value="Save" name="btn" style="height:30px;border-radius:10px;width:80px" onClick="return validateFields()"/>


						<input type="reset" value="Clear" style="margin-left: 10px;height:30px;width:80px;border-radius:10px"/>
						</td>
				</tr>
			</table>


		</form>
	</div>
	</div>

</body>


</html>
