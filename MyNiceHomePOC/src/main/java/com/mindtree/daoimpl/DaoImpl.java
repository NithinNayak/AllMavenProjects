package com.mindtree.daoimpl;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.dao.Dao;
import com.mindtree.entity.HostPropertyDetails;
import com.mindtree.entity.PropertyFacilities;
import com.mindtree.entity.PropertyImages;
@Component
public class DaoImpl implements Dao{
	@Autowired
	private SessionFactory sessionFactory;
	@Transactional
    public void addPropertyDetails(HostPropertyDetails hostPropertyDetails,String[] facilities,String[] images){  
          Session session=sessionFactory.getCurrentSession();
          for(int i=0;i<facilities.length;i++)
          {
        	  PropertyFacilities propertyFacilities=new PropertyFacilities();
        	  propertyFacilities.setFacility(facilities[i]);
        	  propertyFacilities.setHostPropertyDetailsFacilities(hostPropertyDetails);
        	  hostPropertyDetails.getFacilities().add(propertyFacilities);
        	  session.save(propertyFacilities);
          }
          for(int i=0;i<images.length;i++)
          {
        	  PropertyImages propertyImages=new PropertyImages();
        	  propertyImages.setImages(images[i]);
        	  propertyImages.setHostPropertyDetailsImages(hostPropertyDetails);
        	  hostPropertyDetails.getPropertyImages().add(propertyImages);
        	  session.save(propertyImages);
          }
          session.save(hostPropertyDetails);
          
    }
	@Transactional
	public List<String> fetchPropertyNames()
	{
		Session session=sessionFactory.getCurrentSession();
		Query query=session.createQuery("select h.name from HostPropertyDetails h");
		List<String> propertyNames=query.list();
		return propertyNames;
	}
	/*@Transactional
	public List<Object> fetchPropertyDetails()
	{
		
	}*/
	@Transactional
	public void propertyToBeDeleted(String propertyToBeDeleted)
	{
		Session session=sessionFactory.getCurrentSession();
		Query query=session.createQuery("from HostPropertyDetails h where h.name='" + propertyToBeDeleted + "'");
		HostPropertyDetails hostPropertyDetails=(HostPropertyDetails)query.list().get(0);
		session.delete(hostPropertyDetails);
		System.out.println("Deleted");
	}
}
