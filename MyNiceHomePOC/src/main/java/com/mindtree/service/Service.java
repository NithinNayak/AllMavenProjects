package com.mindtree.service;

import java.util.List;

public interface Service {
void addPropertyDetails(String nameOfProp,String address,String city,String state,int noofrooms,int capacity,String aboutfamily,String[] facilities,double cost,String[] images);
List<String> fetchPropertyNames();
/*List<Object> fetchPropertyDetails();*/
void deleteProperty(String propertyToBeDeleted);
}
