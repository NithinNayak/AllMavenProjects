package com.mindtree.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindtree.dao.Dao;
import com.mindtree.entity.HostPropertyDetails;
import com.mindtree.service.Service;
@Component
public class ServiceImpl implements Service{
	@Autowired
	Dao dao;
	public void addPropertyDetails(String nameOfProp,String address,String city,String state,int noofrooms,int capacity,String aboutfamily,String[] facilities,double cost,String[] images)
	{
		HostPropertyDetails hostPropertyDetails=new HostPropertyDetails();
		hostPropertyDetails.setName(nameOfProp);
		hostPropertyDetails.setAddress(address);
		hostPropertyDetails.setCity(city);
		hostPropertyDetails.setState(state);
		hostPropertyDetails.setRooms(noofrooms);
		hostPropertyDetails.setCapacity(capacity);
		hostPropertyDetails.setAboutFamily(aboutfamily);
		hostPropertyDetails.setCost(cost);
		dao.addPropertyDetails(hostPropertyDetails,facilities,images);
	}
	public List<String> fetchPropertyNames()
	{
		return(dao.fetchPropertyNames());
	}
	/*public List<Object> fetchPropertyDetails()
	{
		return(dao.fetchPropertyDetails());
	}*/
	public void deleteProperty(String propertyToBeDeleted)
	{
		dao.propertyToBeDeleted(propertyToBeDeleted);
	}

}
