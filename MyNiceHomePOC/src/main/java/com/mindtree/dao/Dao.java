package com.mindtree.dao;

import java.util.List;

import com.mindtree.entity.HostPropertyDetails;

public interface Dao {
	void addPropertyDetails(HostPropertyDetails hostPropertyDetails,String[] facilities,String[] images);
	 List<String> fetchPropertyNames();
	 /*List<Object> fetchPropertyDetails();*/
	 void propertyToBeDeleted(String propertyToBeDeleted);
}
