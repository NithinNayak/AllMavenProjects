package com.mindtree.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class PropertyFacilities {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int fId;
	String facility;
	@ManyToOne
	HostPropertyDetails hostPropertyDetailsFacilities;
	public int getfId() {
		return fId;
	}
	public void setfId(int fId) {
		this.fId = fId;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public HostPropertyDetails getHostPropertyDetailsFacilities() {
		return hostPropertyDetailsFacilities;
	}
	public void setHostPropertyDetailsFacilities(HostPropertyDetails hostPropertyDetailsFacilities) {
		this.hostPropertyDetailsFacilities = hostPropertyDetailsFacilities;
	}
	
	
}
