package com.mindtree.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;

@Entity
public class HostPropertyDetails {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int pId;
	String name;
	String address;
	String city;
	String state;
	int rooms;
	int capacity;
	String aboutFamily;
	double cost;
	@OneToMany(mappedBy="hostPropertyDetailsFacilities",cascade=CascadeType.REMOVE)
	List<PropertyFacilities> facilities=new ArrayList<PropertyFacilities>();
	@OneToMany(mappedBy="hostPropertyDetailsImages",cascade=CascadeType.REMOVE)
	List<PropertyImages> propertyImages=new ArrayList<PropertyImages>();
	public int getpId() {
		return pId;
	}
	public void setpId(int pId) {
		this.pId = pId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getRooms() {
		return rooms;
	}
	public void setRooms(int rooms) {
		this.rooms = rooms;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public String getAboutFamily() {
		return aboutFamily;
	}
	public void setAboutFamily(String aboutFamily) {
		this.aboutFamily = aboutFamily;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public List<PropertyFacilities> getFacilities() {
		return facilities;
	}
	public void setFacilities(List<PropertyFacilities> facilities) {
		this.facilities = facilities;
	}
	public List<PropertyImages> getPropertyImages() {
		return propertyImages;
	}
	public void setPropertyImages(List<PropertyImages> propertyImages) {
		this.propertyImages = propertyImages;
	}
	
	
	
}
