package com.mindtree.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class PropertyImages {
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
int iId;
String images;
@ManyToOne
HostPropertyDetails hostPropertyDetailsImages;
public int getiId() {
	return iId;
}
public void setiId(int iId) {
	this.iId = iId;
}
public String getImages() {
	return images;
}
public void setImages(String images) {
	this.images = images;
}
public HostPropertyDetails getHostPropertyDetailsImages() {
	return hostPropertyDetailsImages;
}
public void setHostPropertyDetailsImages(HostPropertyDetails hostPropertyDetailsImages) {
	this.hostPropertyDetailsImages = hostPropertyDetailsImages;
}

}
