package com.mindtree.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mindtree.service.Service;

@Controller
public class MainController {
	@Autowired
	Service service;
	String nameOfProp,address,city,state,aboutfamily;
	double cost;
	int capacity,noofrooms;
	String[] facilities,images;
	@RequestMapping(value = "/addProperty")
	public String displayAddProperty()
	{
		return "addProperty";
	}
	@RequestMapping(value = "/index")
	public String displayHomePage()
	{
		return "index";
	}
	
	@RequestMapping(value = "/addProperty", method = RequestMethod.POST)
	public ModelAndView addProperty(HttpServletRequest request,HttpServletResponse response)
	{
		nameOfProp=request.getParameter("propName");
		address=request.getParameter("address");
		city=request.getParameter("city");
		state=request.getParameter("state");
		noofrooms=Integer.parseInt(request.getParameter("noofrooms"));
		capacity=Integer.parseInt(request.getParameter("capacity"));
		aboutfamily=request.getParameter("aboutfamily");
		facilities=request.getParameterValues("facilities");
		for(String fac:facilities)
		{
			System.out.println(fac);
		}
		cost=Double.parseDouble(request.getParameter("cost"));
		images=request.getParameterValues("images");
		service.addPropertyDetails(nameOfProp,address,city,state,noofrooms,capacity,aboutfamily,facilities,cost,images);
		ModelAndView model=new ModelAndView("index");
		model.addObject("msg", "Property added successfully");
		return model;
		
	}
	@RequestMapping(value = "/editProperty")
	public ModelAndView displayEditProperty()
	{
		ModelAndView model=new ModelAndView("editProperty");
		List<String> propertyNames=service.fetchPropertyNames();
		for(String res:propertyNames)
		{
			System.out.println(res);
		}
		model.addObject("propertyNames", propertyNames);
		return model;
	}
	/*@RequestMapping(value = "/propertyToBeEdited")
	public ModelAndView propertyToBeEdited(HttpServletRequest request,HttpServletResponse response)
	{
		
		ModelAndView model=new ModelAndView("propertyToBeEdited");
		String propertyName=
		List<Object> propertyNames=service.fetchPropertyDetails();
		for(Object res:propertyNames)
		{
			System.out.println(res);
		}
		model.addObject("propertyNames", propertyNames);
		return model;
	}*/
	@RequestMapping(value = "/deleteProperty")
	public ModelAndView displayDeleteProperty()
	{
		ModelAndView model=null;
		List<String> propertyNames=service.fetchPropertyNames();
		if(propertyNames.isEmpty())
		{
			model=new ModelAndView("index");
			model.addObject("msg", "No properties found");
		}
		else
		{
			model=new ModelAndView("deleteProperty");
			model.addObject("propertyNames", propertyNames);
		}
		return model;
	}
	@RequestMapping(value = "/propertyToBeDeleted", method = RequestMethod.POST)
	public ModelAndView deleteProperty(HttpServletRequest request,HttpServletResponse response)
	{
		String propertyToBeDeleted=request.getParameter("propertyName");
		System.out.println("In delete");
		System.out.println(propertyToBeDeleted);
		service.deleteProperty(propertyToBeDeleted);
		ModelAndView model=new ModelAndView("index");
		model.addObject("msg", "Property deleted successfully");
		return model;
		
	}
	
	

}
