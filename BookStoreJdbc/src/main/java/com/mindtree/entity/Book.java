package com.mindtree.entity;

import org.omg.CORBA.SystemException;

public class Book implements Comparable{
	private int bookId;
	private String bookName;
	private String authorName;
	private String publisherName;
	private String categoryName;
	private int price;
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getPublisherName() {
		return publisherName;
	}
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String toString()
	{
		return this.getBookId()+" "+this.getBookName()+" "+this.getAuthorName()+" "+this.getPublisherName()+" "+this.getPrice();
	}
	public int compareTo(Object ob)
	{
		if((getBookName()).compareTo(((Book)(ob)).getBookName())>1)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
	

}
