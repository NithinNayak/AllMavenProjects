package com.mindtree.entity;

import java.util.Date;

public class Purchase {
	Book book;
private String customerName;
private String customerMobileNo;
private Date purchaseDate;
private int amount;
private int bookId;
public Book getBook() {
	return book;
}
public void setBook(Book book) {
	this.book = book;
}
public String getCustomerName() {
	return customerName;
}
public void setCustomerName(String customerName) {
	this.customerName = customerName;
}
public String getCustomerMobileNo() {
	return customerMobileNo;
}
public void setCustomerMobileNo(String customerMobileNo) {
	this.customerMobileNo = customerMobileNo;
}
public Date getPurchaseDate() {
	return purchaseDate;
}
public void setPurchaseDate(Date purchaseDate) {
	this.purchaseDate = purchaseDate;
}
public int getAmount() {
	return amount;
}
public void setAmount(int amount) {
	this.amount = amount;
}
public int getBookId() {
	return bookId;
}
public void setBookId(int bookId) {
	this.bookId = bookId;
}

}
