package com.mindtree.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

import com.mindtree.daoimpl.DaoImpl;
import com.mindtree.entity.Book;
import com.mindtree.entity.Purchase;
import com.mindtree.exceptions.InvalidBookIdException;
import com.mindtree.exceptions.InvalidMobileNoException;
import com.mindtree.exceptions.IvalidCategoryException;

public class BookStoreSystemManager {
	DaoImpl d=new DaoImpl();
	public ResultSet res;
	TreeSet<Book> ts=new TreeSet<Book>();
	public void displayBookDetails(Book b)
	{
		try{
		Scanner scan=new Scanner(System.in);
		do{
		System.out.print("Enter the category [");
		res=d.retrieveCategory();
		while(res.next())
			System.out.print(res.getString(1)+" ");
		System.out.print("]: ");
		b.setCategoryName(scan.next());}while(!isValidCategory(b.getCategoryName()));
		res=d.retrieveBookDetails(b.getCategoryName());
		while(res.next())
		{
			Book b1=new Book();
		b1.setBookId(res.getInt(1));
		b1.setBookName(res.getString(2));
		b1.setAuthorName(res.getString(3));
		b1.setPublisherName(res.getString(4));
		b1.setPrice(res.getInt(5));
		ts.add(b1);
		}
		System.out.println("Id"+"      "+"Name"+"                         "+"Author"+"    "+"Publisher"+"     "+"Price");//use t. instd to print individual elmts
		for(Book t:ts)
			System.out.println(t);
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("Class failure");
		}
	}
	public void purchaseABook(Purchase p)
	{
		try{
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter book id: ");
		do{
		    p.setBookId(scan.nextInt());
		    }while(!isValidBookId(p.getBookId()));
		scan.nextLine();
		System.out.println("Enter customer name: ");
		do{
		p.setCustomerName(scan.nextLine());}while(!isValidCustomerName(p.getCustomerName()));
		System.out.println("Enter customer mobile number: ");
		do{
			p.setCustomerMobileNo(scan.next());
		}while(!isValidMobileNum(p.getCustomerMobileNo()));
		System.out.println("Book purchased successfully");
		d.insertIntoTble(p.getBookId(),p.getCustomerName(),p.getCustomerMobileNo());
		System.out.println("Purchase number:");
		res=d.retrivPurchseNo();
		while(res.next())
		{
			
			System.out.print(res.getInt(1));
		}
		System.out.println("purchase date: ");
		Date date=java.util.Calendar.getInstance().getTime();  
		System.out.println(date);
		p.setPurchaseDate(date);
		res=d.retrvBookPrice(p.getBookId());
		while(res.next())
		{
			p.setAmount(res.getInt(1));
		}
		System.out.println(p.getAmount());
		d.updtTble(p.getAmount(),p.getBookId());
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("Class failure");
		}
	}
	
	
	public boolean isValidCategory(String category) throws ClassNotFoundException, SQLException
	{
		try{
		res=d.retrieveCategory();
		while(res.next())
		{
			if(category.equals(res.getString(1)))
				return true;
		}
		throw new  IvalidCategoryException();}
		catch(IvalidCategoryException e)
		{
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	public boolean isValidBookId(int bookId) throws ClassNotFoundException, SQLException
	{
			res=d.retrieveBookId();
			try{
			while(res.next())
			{
				if(bookId==(res.getInt(1)))
					return true;
			}
			throw new IvalidCategoryException();
			}
			catch (IvalidCategoryException e) {
				System.out.println(e.getMessage());
				return false;
			}
	}
	public boolean isValidCustomerName(String customerName)
	{
		if(!Pattern.matches("[A-Za-z\\s]+", customerName))
		{
			System.out.println("Wrong Customer Name.Enter again");
			return false;
		}
		else
		{
			return true;
		}
	}
	public boolean isValidMobileNum(String customerMobileNum)
	{
		try{
		if(!Pattern.matches("[789][0-9]{9}+", customerMobileNum))
		{
			throw new InvalidMobileNoException();
		}
		else
		{
			return true;
		}
		}
		catch(InvalidMobileNoException e)
		{
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	

}