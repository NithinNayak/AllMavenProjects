package com.mindtree.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnector {
	public static Connection con = null;
	static String username = "root";
	static String password = "Welcome123";
	static String url = "jdbc:mysql://localhost:3306/application";
	static String drivrClasPath = "com.mysql.jdbc.Driver";

	public static Connection dbConnection() throws SQLException, ClassNotFoundException {
		Class.forName(drivrClasPath);
		try {
			con = DriverManager.getConnection(url, username, password);
			return con;
		} catch (Exception e) {
			throw new SQLException("Could not establish the connection with database");
		}
	}

}
