package com.mindtree.client;

import java.util.Scanner;

import com.mindtree.entity.Book;
import com.mindtree.entity.Purchase;
import com.mindtree.manager.BookStoreSystemManager;

public class BookStoreSystemClient {

	public static void main(String[] args) {
		String input="0";
		Book b=new Book();
		Purchase p=new Purchase();
		BookStoreSystemManager bssm=new BookStoreSystemManager();
		// TODO Auto-generated method stub
		while (true) {
			Scanner scan = new Scanner(System.in);
			if (Integer.parseInt(input) == 0) {
				System.out.println("XYZ Book Store");
				System.out.println("-----------------------");
				System.out.println("1. Display book details");
				System.out.println("2. Purchase a book");
				System.out.println("3. Exit");
				System.out.print("Enter the choice: ");
				input = scan.next();
			}
			switch (Integer.parseInt(input)) {
			case 1:
				System.out.println("-----------------------");
				System.out.println("Available book categories");
				System.out.println("-----------------------");
				bssm.displayBookDetails(b);
				break;
			case 2:
				System.out.println("-----------------------");
				System.out.println("Purchase Book");
				System.out.println("-----------------------");
				bssm.purchaseABook(p);
				break;
			case 3:
				System.out.println("You have been successfully exited from the app");
				System.exit(0);
			default:
				System.out.println("You have entered a wrong choice");
				break;
			}
			input = "0";
			scan.nextLine();
		}

	}

}
