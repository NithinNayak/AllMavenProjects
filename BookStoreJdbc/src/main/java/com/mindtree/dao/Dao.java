package com.mindtree.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Dao {
	public ResultSet retrieveCategory() throws ClassNotFoundException, SQLException;
	public ResultSet retrieveBookDetails(String category) throws SQLException, ClassNotFoundException;
	public ResultSet retrieveBookId() throws ClassNotFoundException, SQLException;
	public ResultSet retrivPurchseNo() throws ClassNotFoundException, SQLException;
	public ResultSet retrvBookPrice(int bookId) throws ClassNotFoundException, SQLException;
	public void insertIntoTble(int bookId,String customerName,String customerMobileNo) throws ClassNotFoundException, SQLException;
	public void updtTble(int price,int bookId) throws ClassNotFoundException, SQLException;

}
