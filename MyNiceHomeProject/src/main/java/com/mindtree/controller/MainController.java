package com.mindtree.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.dto.BookingInformation;
import com.mindtree.service.Service;
@CrossOrigin
@RestController
public class MainController {
	@Autowired
	Service service;
	@RequestMapping(value="/traveller" , method=RequestMethod.POST)
	public List<BookingInformation> getBookingInfo(@RequestBody String id)
	{
		return(service.fetchBookingDetails(Integer.parseInt(id)));
	}
	@RequestMapping(value="/delete" , method=RequestMethod.POST)
	public void deleteBookingInfo(@RequestBody String id)
	{
		service.deleteBookingDetails(Integer.parseInt(id));
	}
		
		
}
/*@RequestMapping(value="/delete/{id}" , method=RequestMethod.GET)
public void deleteBookingInfo(@PathVariable("id") String id)
{
	System.out.println(id);
}*/

//here
/*@RequestMapping(value="/traveller" , method=RequestMethod.GET)
public List<TourOperator> getBookingInfo()
{
	System.out.println("here");
	ArrayList<TourOperator> arr=new ArrayList<TourOperator>();
	System.out.println("Hi");
	TourOperator t=new TourOperator();
	t.setTravelId(1);
	t.setName("B");
	t.setMob("999");
	t.setEmail("nayak");
	t.setCheckin("Check in");
	t.setCheckout("Checkout");
	arr.add(t);
	return arr;
	
	List<TourOperator> tourOperator=service.fetchBookingDetails();
	return tourOperator;
}
@RequestMapping(value="/delete" , method=RequestMethod.POST)
public void deleteBookingInfo(@RequestBody String id)
{
	service.deleteBookingDetails(Integer.parseInt(id));
}*/
