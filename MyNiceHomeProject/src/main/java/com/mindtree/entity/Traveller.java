package com.mindtree.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Traveller {
	@Id
	int tid;
	String name;
	String email;
	String mobile;
	String gender;
	String password;
	@Override
	public String toString() {
		return "Traveller [tid=" + tid + ", name=" + name + ", email=" + email + ", mobile=" + mobile + ", gender="
				+ gender + ", password=" + password + "]";
	}
	
	

}
