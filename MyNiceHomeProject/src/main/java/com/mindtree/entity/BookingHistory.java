package com.mindtree.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class BookingHistory {
	@Id
	int bookingid;
	int propid;
	String checkindate;
	String checkoutdate;
	String email;
	String noofguests;
	String noofrooms;
	@Override
	public String toString() {
		return "BookingHistory [bookingid=" + bookingid + ", propid=" + propid + ", checkindate=" + checkindate
				+ ", checkoutdate=" + checkoutdate + ", email=" + email + ", noofguests=" + noofguests + ", noofrooms="
				+ noofrooms + "]";
	}
	

}
