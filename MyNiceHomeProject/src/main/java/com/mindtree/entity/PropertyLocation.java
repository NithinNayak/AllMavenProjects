package com.mindtree.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PropertyLocation {
	@Id
	int propid;
	String name;
	String address;
	String city;
	String state;
	String hostid;
	@Override
	public String toString() {
		return "PropertyLocation [propid=" + propid + ", name=" + name + ", address=" + address + ", city=" + city
				+ ", state=" + state + ", hostid=" + hostid + "]";
	}
	
}
