package com.mindtree.dao;

import java.util.List;

import com.mindtree.dto.BookingInformation;

public interface Dao {
	public List<BookingInformation> fetchBookingDetails(int id);
	public void deleteBookingDetails(int id);

}
