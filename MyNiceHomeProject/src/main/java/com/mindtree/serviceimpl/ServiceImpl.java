package com.mindtree.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindtree.dao.Dao;
import com.mindtree.dto.BookingInformation;
import com.mindtree.service.Service;
@Component
public class ServiceImpl implements Service{
	@Autowired
	Dao dao;

	public List<BookingInformation> fetchBookingDetails(int id) {
	return(dao.fetchBookingDetails(id));	
	}

	public void deleteBookingDetails(int id) {
		dao.deleteBookingDetails(id);
	}
	

}
