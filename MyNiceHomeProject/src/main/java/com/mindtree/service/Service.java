package com.mindtree.service;

import java.util.List;

import com.mindtree.dto.BookingInformation;


public interface Service {
	List<BookingInformation> fetchBookingDetails(int id);
	void deleteBookingDetails(int id);

}
