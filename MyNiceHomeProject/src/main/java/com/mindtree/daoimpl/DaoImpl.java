package com.mindtree.daoimpl;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.dao.Dao;
import com.mindtree.dto.BookingInformation;
import com.mindtree.entity.BookingHistory;
@Component
@Transactional
public class DaoImpl implements Dao{
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<BookingInformation> fetchBookingDetails( int id)
	{
		List<BookingInformation> li=new ArrayList<BookingInformation>();
		Session session=sessionFactory.getCurrentSession();
		Query query=session.createQuery("select bh.bookingid,t.name,t.email,t.mobile,pl.name,bh.checkindate,bh.checkoutdate,bh.noofguests,bh.noofrooms from PropertyLocation pl,BookingHistory bh,Traveller t where t.email=bh.email and bh.propid=pl.propid and pl.hostid='"+id+"'");
		List res = query.list();
		Iterator it = res.iterator();
		while (it.hasNext()) {
			BookingInformation bookingInformation=new BookingInformation();
			Object row[] = (Object[]) it.next();
			bookingInformation.setBookingId((Integer)row[0]);
			bookingInformation.setTravellerName(String.valueOf(row[1]));
			bookingInformation.setEmail(String.valueOf(row[2]));
			bookingInformation.setMobile(String.valueOf(row[3]));
			bookingInformation.setPropertyName(String.valueOf(row[4]));
			bookingInformation.setCheckInDate(String.valueOf(row[5]));
			bookingInformation.setCheckOutDate(String.valueOf(row[6]));
			bookingInformation.setNoofguests(String.valueOf(row[7]));
			bookingInformation.setNoofrooms(String.valueOf(row[8]));
			li.add(bookingInformation);
			/*System.out.println(bookingInformation.getBookingId()+" "+bookingInformation.getTravellerName());*/
			/*System.out.println(row[0]+" "+row[1]+" "+row[2]+" "+row[3]+" "+row[4]+" "+row[5]+" "+row[6]+" "+row[7]+" "+row[8]);*/
			/*Object row=(Object)it.next();
			System.out.println(row);*/
		}
		return li;

	}
	public void deleteBookingDetails(int id) {
		Session session=sessionFactory.getCurrentSession();
		Query query=session.createQuery("from BookingHistory b where b.bookingid='"+id+"'");
		BookingHistory bookingHistory=(BookingHistory)query.list().get(0);
		session.delete(bookingHistory);
		System.out.println("Deleted");
	}
	

}
/*"from PropertyLocation pl join BookingHistory bh join Traveller t  t.email=bh.email where pl.hostid='"+id+"'"*/
