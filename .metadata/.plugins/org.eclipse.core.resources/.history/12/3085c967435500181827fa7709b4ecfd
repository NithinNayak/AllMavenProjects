package com.mindtree.mynicehome.servicetest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mindtree.mynicehome.configuration.ConfigurationClass;
import com.mindtree.mynicehome.dao.MyNiceHomeDao;
import com.mindtree.mynicehome.dto.BookingInformation;
import com.mindtree.mynicehome.dto.Filter;
import com.mindtree.mynicehome.dto.MailBodyDetails;
import com.mindtree.mynicehome.dto.PropertyCity;
import com.mindtree.mynicehome.dto.PropertyDet;
import com.mindtree.mynicehome.entity.BookingHistory;
import com.mindtree.mynicehome.entity.PackageDetails;
import com.mindtree.mynicehome.entity.Person;
import com.mindtree.mynicehome.entity.PropertyDetails;
import com.mindtree.mynicehome.entity.PropertyFacilities;
import com.mindtree.mynicehome.entity.PropertyLocation;
import com.mindtree.mynicehome.entity.User;
import com.mindtree.mynicehome.exceptions.MyNiceHomeException;
import com.mindtree.mynicehome.exceptions.NoPackageAddedException;
import com.mindtree.mynicehome.exceptions.SomethingWentWrongException;
import com.mindtree.mynicehome.serviceimpl.MyNiceHomeServiceImpl;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {ConfigurationClass.class})
public class ServiceTest {
	@Mock
	private MyNiceHomeDao dao;
	
	@InjectMocks
	private MyNiceHomeServiceImpl service;
	

	@Spy
	private List<PropertyFacilities> facilities=new ArrayList<PropertyFacilities>();
	
	@Spy
	private List<BookingHistory> history=new ArrayList<BookingHistory>();
	
	@Spy
	private static List<PropertyDet> det=new ArrayList<PropertyDet>();
	
	String email="himaja@gmail.com";
	
	@Spy
	private ArrayList<PropertyLocation> props=new ArrayList<PropertyLocation>();
	
	java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
	String city="banglore";
	int guests=4;
	String price="More than 2000";
	
	@Spy
	private List<BookingInformation> bookingInformation=new ArrayList<BookingInformation>();
	int hostId;
	int bookingId;
	int tourId;
	String travellerName;
	MailBodyDetails mailBodyDetails;
	PackageDetails pd;
	
	@BeforeClass
	public void testSetUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		facilities=getDummyFacilities(1);
		when(dao.getPropertyFacilities(1)).thenReturn(facilities);
		
		history=getDummyHistory(email);		
		when(dao.getBookingHistory(email)).thenReturn(history);
		
		det=getDummyList(city,date,guests);
		when(dao.getList(city,date,guests,price)).thenReturn(det);
		
		pd=new PackageDetails();
		bookingId=1;
		hostId=1;
		tourId=1;
		email="nayaknithin506@gmail.com";
		travellerName="Himaja";
		bookingInformation=getBookingDetails(hostId);
		mailBodyDetails=getMailDetails();
	}
	
	
	
	@Test
	public void getFacilities() {
		when(dao.getPropertyFacilities(1)).thenReturn(facilities);		
		assertEquals(service.getPropertyFacilitiesService(1).size(),1);
	}
	
	public ArrayList<PropertyFacilities> getDummyFacilities(int propId){
		
		ArrayList<PropertyFacilities> fac=new ArrayList<PropertyFacilities>();
		PropertyFacilities pf=new PropertyFacilities();
		pf.setfId(1);
		pf.setFacility("WiFi");
		pf.setPropId(1);
		fac.add(pf);
		return fac;
	}
	
	@Test
	public void getHistory() {
		when(dao.getBookingHistory(email)).thenReturn(history);		
		assertEquals(service.getBookingHistoryService(email).size(),1);
	}
	
	
	public ArrayList<BookingHistory> getDummyHistory(String email){
		PropertyLocation pl=new PropertyLocation();
		ArrayList<BookingHistory> hist=new ArrayList<BookingHistory>();
		BookingHistory bh=new BookingHistory();
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		bh.setCheckInDate(date);
		bh.setCheckOutDate(date);
		bh.setGuests(2);
		bh.setProperty(pl);
		hist.add(bh);
		return hist;
	}
	
	@Test
	public void getLocation() {
		int id=1;
		PropertyLocation pl=new PropertyLocation();
		pl.setAddress("fdg");
		pl.setCity("hfgh");
		pl.setHostId(1);
		pl.setName("chng");
		pl.setState("gfh");
		try {
			when(dao.getPropertyLocation(id)).thenReturn(pl);
		} catch (MyNiceHomeException e) {
			
		}
		assertEquals(service.getPropertyLocationService(id).getName(),"chng");
	}
	
	@Test
	public void loginTest() {
		User u=new User();
		u.setUname("himaja");
		u.setPassword("1234");
		u.setAsa("host");
		when(dao.doLogin(u)).thenReturn(1);
		assertEquals(service.loginService(u),1);
	}
	
	@Test
	public void checkIdTest() {
		User u=new User();
		u.setUname("himaja@gmail.com");
		u.setPassword("1234");
		u.setAsa("host");
		try {
			when(dao.checkEmailId(u)).thenReturn(1234);
		} catch (MyNiceHomeException e) {
			
		}
		assertEquals(service.checkIdService(u),1234);
	}
	@Test
	public void updatePwdTest() {
		User u=new User();
		u.setUname("abc@gmail.com");
		u.setPassword("1234");
		u.setAsa("host");
		when(dao.doUpdatePassword(u)).thenReturn(1);
		assertEquals(service.updatePasswordService(u),1);
	}
	
	@Test
	public void registerTest() throws Exception {
		Person p=new Person();
		p.setAsa("Tour Operator");
		p.setEmail("ab@gmail.com");
		p.setGender("Female");
		p.setId(2);
		p.setMobile("98989898");
		p.setName("ab");
		p.setPassword("123");
		when(dao.doRegister(p)).thenReturn(1);
		assertEquals(service.RegisterService(p),1);
	}
	
	@Test
	public void getDetails() throws MyNiceHomeException {
		int id=1;
		PropertyDetails pd=new PropertyDetails();
		pd.setRooms(4);
		pd.setCapacity(8);
		pd.setCost(1000);
		pd.setAboutFamily("couple");
		when(dao.getPropertyDetails(id)).thenReturn(pd);
		assertEquals(service.getPropertyDetailsService(id).getCapacity(),8);
	}
	
	@Test
	public void getList() {
		try {
			when(dao.getList(city,date,guests,price)).thenReturn(det);
		} catch (MyNiceHomeException e) {
		
		}		
		assertEquals(service.getListService(city, date, guests,price).size(),1);
	}
	public List<PropertyDet> getDummyList(String city, java.sql.Date date, int guests) {
		ArrayList<PropertyDet> properties=new ArrayList<PropertyDet>();
		PropertyDet pd=new PropertyDet();
			pd.setPropId(1);
			pd.setAddress("abc");
			pd.setCity("hyd");
			pd.setState("ap");
			pd.setName("home1");
			pd.setPrice(1000);
			properties.add(pd);
		
		return properties;		
	}
	
	@Test
	public void checkPropTest() {
		PropertyLocation pl=new PropertyLocation();
		pl.setAddress("3/21");
		pl.setCity("Bangalore");
		pl.setHostId(1);
		pl.setName("Home1");
		pl.setState("Karnataka");
		when(dao.checkProp(pl)).thenReturn(1);
		assertEquals(service.checkPropService(pl),1);
	}
	
	@Test
	public void insertLocTest() {
		PropertyLocation pl=new PropertyLocation();
		pl.setAddress("3B");
		pl.setCity("Bangalore");
		pl.setHostId(1);
		pl.setName("Home8");
		pl.setState("Karnataka");
		try {
			when(dao.insertLocation(pl)).thenReturn("Added");
		} catch (MyNiceHomeException e) {
			
		}
		assertEquals(service.insertLocationService(pl),"Added");
	}
	
	@Test
	public void updateLocTest() {
		PropertyLocation pl=new PropertyLocation();
		pl.setAddress("3B");
		pl.setCity("Bangalore");
		pl.setHostId(1);
		pl.setName("Home8");
		pl.setState("Karnataka");
		when(dao.updatePropLocation(pl)).thenReturn("updated");
		assertEquals(service.updatePropLocationService(pl),"updated");
	}
	
	@Test
	public void insertPropDetailsTest() {
		PropertyDetails pd=new PropertyDetails();
		pd.setPropId(5);
		pd.setRooms(5);
		pd.setCapacity(4);
		pd.setAboutFamily("couple");
		pd.setCost(1000);
		when(dao.insertDetails(pd)).thenReturn("Added");
		assertEquals(service.insertDetailsService(pd),"Added");
	}
	
	@Test
	public void updatePropDetailsTest() {
		PropertyDetails pd=new PropertyDetails();
		pd.setPropId(5);
		pd.setRooms(5);
		pd.setCapacity(4);
		pd.setAboutFamily("couple");
		pd.setCost(1000);
		when(dao.updatePropDetails(pd)).thenReturn("updated");
		assertEquals(service.updatePropDetailsService(pd),"updated");
	}
	
	@Test
	public void insertFacilitiesTest() {
		String[] facilities= {"Food","WiFi"};
		when(dao.insertFacilities(facilities)).thenReturn("Added");
		assertEquals(service.insertFacilitiesService(facilities),"Added");
	}
	
	@Test
	public void deletePropTest() {
		int propId=1;
		when(dao.delete(propId)).thenReturn(1);
		assertEquals(service.deleteService(propId),1);
	}
	
	@Test
	public void cancelTest() {
		int id=1;
		when(dao.cancelBooking(id)).thenReturn("Cancelled");
		assertEquals(service.cancelBookingService(id),"Cancelled");
	}
	
	@Test
	public void fetchBookingTest() {
		int bookingId=1;
		BookingInformation info=new BookingInformation();
		List<BookingInformation> booking=new ArrayList<BookingInformation>();
		info.setBookingId(1);
		info.setCheckInDate("2018-06-05");
		info.setCheckOutDate("2018-06-07");
		info.setEmail("abc@gmail.com");
		info.setMobile("9999999999");
		info.setTravellerName("abc");
		info.setPropertyName("Prop1");
		info.setNoofguests("2");
		booking.add(info);
			try {
				when(dao.fetchBookingDetails(bookingId)).thenReturn(booking);
				assertEquals(service.fetchBookingDetails(bookingId).size(),1);
			} catch (SomethingWentWrongException e) {
			}
	}
	
	@Test
	public void getCityTest() {
		List<PropertyCity> cities=new ArrayList<PropertyCity>();
		PropertyCity city=new PropertyCity();
		city.setCity("Bangalore");
		cities.add(city);
		when(dao.getCity()).thenReturn((ArrayList<PropertyCity>) cities);
		assertEquals(service.getCityService().size(),1);
	}
	
	@Test
	public void deleteBookingTest() {
		int bookingId=1;
		try {
			when(dao.deleteBookingDetails(bookingId)).thenReturn("Booking Deleted");
			assertEquals(service.deleteBookingDetails(bookingId),"Booking Deleted");
		} catch (SomethingWentWrongException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void confirmTest() {
		PropertyLocation pl=new PropertyLocation();
		pl.setAddress("3B");
		pl.setCity("Bangalore");
		pl.setHostId(1);
		pl.setName("Home8");
		pl.setState("Karnataka");
		BookingHistory booking=new BookingHistory();
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		booking.setBookingId(1);
		booking.setCheckInDate(date);
		booking.setCheckOutDate(date);
		booking.setEmail("himaja@gmail.com");
		booking.setGuests(3);
		booking.setProperty(pl);
		when(dao.confirmBooking(booking)).thenReturn("Confirmed");
		assertEquals(service.confirmBookingService(booking),"Confirmed");
	}
	
	@Test
	public void filterTest() {
		Filter filter=new Filter();
		ArrayList<String> al=new ArrayList<String>();
		al.add("WiFi");
		al.add("Food");
		al.add("More than 2000");
		filter.setAl(al);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		filter.setCkin(date);
		filter.setCkot(date);
		filter.setDest("Bangalore");
		filter.setNog("2");
		ArrayList<PropertyDet> propDet=new ArrayList<PropertyDet>();
		PropertyDet details=new PropertyDet();
		details.setAddress("3/21");
		details.setCity("Bangalore");
		details.setName("Sweet Home");
		details.setPropId(1);
		details.setState("Karnataka");
		propDet.add(details);
		try {
			when(dao.getFilterList(filter)).thenReturn(propDet);
		} catch (MyNiceHomeException e) {
			
		}
		assertEquals(service.getFilterListService(filter).size(),1);
	}
	
	@Test
	public void fetchBookingDetailsPositiveServiceTest()
	{
		try {
			when(dao.fetchBookingDetails(hostId)).thenReturn(bookingInformation);
			assertEquals(service.fetchBookingDetails(hostId).size(),2);
		} catch (SomethingWentWrongException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test(expectedExceptions=SomethingWentWrongException.class)
	public void fetchBookingDetailsNegativeServiceTest() throws SomethingWentWrongException
	{
		int p = 0;
			when(dao.fetchBookingDetails(p)).thenThrow(SomethingWentWrongException("Something went wrong"));
			service.fetchBookingDetails(p);
	}
	@Test
	public void deleteBookingDetailsPositiveServiceTest()
	{
		String msg="Test2";
		try {
			when(dao.deleteBookingDetails(bookingId)).thenReturn(msg);
			assertEquals(service.deleteBookingDetails(bookingId),msg);
		} catch (SomethingWentWrongException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*@Test
	public void deleteBookingDetailsNegativeServiceTest()
	{
		String msg=null;
		try {
			when(dao.deleteBookingDetails(bookingId)).thenThrow(new SomethingWentWrongException("Something went wrong"));
			assertEquals(service.deleteBookingDetails(bookingId),msg);
		} catch (SomethingWentWrongException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	@Test
	public void addPackageDetailsPositiveTest()
	{
		String msg="SUCCESS";
		try {
			when(dao.addPackageDetails(pd)).thenReturn(msg);
			assertEquals(service.addPackageDetails(pd),msg);
		} catch (SomethingWentWrongException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	/*@Test
	public void addPackageDetailsNegativeTest()
	{
		String msg=null;
		try {
			when(dao.addPackageDetails(pd)).thenReturn(msg);
			assertEquals(service.addPackageDetails(pd),msg);
		} catch (SomethingWentWrongException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}*/
	@Test
	public void fetchBookingsTest()
	{
		try {
			when(dao.fetchBookings(tourId)).thenReturn(bookingInformation);
			assertEquals(service.fetchBookings(tourId).size(),2);
		} catch (NoPackageAddedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SomethingWentWrongException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void fetchMailDataTest()
	{
		try {
			when(dao.fetchMailData(tourId)).thenReturn(mailBodyDetails);
			assertEquals(service.fetchMailData(tourId).getAgencyName(),"Sri Ganesh");
		} catch (SomethingWentWrongException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test
	public void sendMailTest()
	{
		String msg="FAILURE";
		assertEquals(service.sendMail(mailBodyDetails, travellerName,email),msg);
	}
	public static List<BookingInformation> getBookingDetails(int hostId)
	{
		List<BookingInformation> bookingInformation=new ArrayList<BookingInformation>();
		BookingInformation b1=new BookingInformation();
		b1.setBookingId(1);
		b1.setCheckInDate("2018/06/06");
		b1.setCheckOutDate("2018/0707");
		b1.setEmail("jyoti.behera@gmail.com");
		b1.setMobile("+919192939495");
		b1.setNoofguests("6");
		b1.setPropertyName("Anandvan");
		b1.setTravellerName("Jyothi Behera");
		bookingInformation.add(b1);
		BookingInformation b2=new BookingInformation();
		b2.setBookingId(2);
		b2.setCheckInDate("2018/02/02");
		b2.setCheckOutDate("2018/03/03");
		b2.setEmail("shanmukh@gmail.com");
		b2.setMobile("+919697989900");
		b2.setNoofguests("2");
		b2.setPropertyName("Brindavan");
		b2.setTravellerName("Shanmukh");
		bookingInformation.add(b2);
		return bookingInformation;
	}
	public static MailBodyDetails getMailDetails()
	{
		MailBodyDetails mailBodyDetails=new MailBodyDetails();
		mailBodyDetails.setAgencyName("Sri Ganesh");
		mailBodyDetails.setAddress("Near Chandan Van");
		mailBodyDetails.setCost("50");
		mailBodyDetails.setMobile("9740692364");
		return mailBodyDetails;
		}

}