package com.mindtree.mynicehome.daotest;

import static org.testng.AssertJUnit.assertEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mindtree.mynicehome.dao.MyNiceHomeDaoImpl;
import com.mindtree.mynicehome.dto.Filter;
import com.mindtree.mynicehome.dto.MailBodyDetails;
import com.mindtree.mynicehome.dto.PropertyCity;
import com.mindtree.mynicehome.entity.BookingHistory;
import com.mindtree.mynicehome.entity.PackageDetails;
import com.mindtree.mynicehome.entity.PropertyFacilities;
import com.mindtree.mynicehome.entity.PropertyLocation;
import com.mindtree.mynicehome.exceptions.MyNiceHomeException;
import com.mindtree.mynicehome.exceptions.SomethingWentWrongException;

public class PropertyDaoTest extends EntityDaoImplTest{

	@Override
	protected IDataSet getDataSet() throws Exception {
		IDataSet dataSet=new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("PropertyDataSet.xml"));
		return dataSet;
	}
	
	@Autowired
	private MyNiceHomeDaoImpl daoImpl;
	
	@Spy
	private static List<PropertyLocation> propLocation=new ArrayList<PropertyLocation>();
	
	
	@Spy
	private static List<PropertyFacilities> propFacilities=new ArrayList<PropertyFacilities>();
	
	@Spy
	private static List<BookingHistory> history=new ArrayList<BookingHistory>();
	
	@Spy
	private static List<BookingHistory> history1=new ArrayList<BookingHistory>();
	PackageDetails packageDetails;
	MailBodyDetails mailBodyDetails;
	int hostId;
	int bookingId;
	int tourId;
	String email;
	 
	@BeforeClass
	public void testSetUp() throws Exception {
		//daoImpl.setSessionFactory(sessionFactory);
		packageDetails=packageDetrails();
		mailBodyDetails=getMailDetails();		
		bookingId=1;
		hostId=1;
		tourId=1;
		email="nayaknithin506@gmail.com";
	}
	
	@Test
	public void getPropertyTest() throws MyNiceHomeException {
		Assert.assertEquals(daoImpl.getPropertyLocation(1).getName(), "Prop1");
	}
	
	@Test
	public void getAllPropertiesTest() {
		assertEquals(daoImpl.getPropDetails1(1).size(),2);
	}
	
	@Test
	public void getPropertyDetailsTest() throws MyNiceHomeException {
		Assert.assertEquals(daoImpl.getPropertyDetails(2).getAboutFamily(), "Old Couple");
	}
	
	@Test
	public void getPropertyFacilitiesTest() {
		assertEquals(daoImpl.getPropertyFacilities(3).size(),3);
	}
	
	@Test
	public void insertFacilitiesTest() {
		String[] facilities= {"Wifi","Food"};
		assertEquals(daoImpl.insertFacilities(facilities),"Added");
	}
	@Test
	public void getBookingHistoryTest() {
		String email="xyz@gmail.com";
		assertEquals(daoImpl.getBookingHistory(email).size(),2);
	}
	
	@Test
	public void getListTest() throws MyNiceHomeException {
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		assertEquals(daoImpl.getList("Bangalore",date,2,"More than 2000").size(),1);
	}
	
	@Test
	public void getListTest2() throws MyNiceHomeException {
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		assertEquals(daoImpl.getList("Bangalore",date,2,null).size(),2);
	}
	
	@Test
	public void getListTest3() throws MyNiceHomeException {
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		assertEquals(daoImpl.getList("Bangalore",date,2,"500-1000").size(),0);
	}
	@Test
	public void getListTest4() throws MyNiceHomeException {
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		assertEquals(daoImpl.getList("Bangalore",date,2,"1000-2000").size(),1);
	}
	
	
	@Test
	public void checkPropTest() {
		PropertyLocation pl=new PropertyLocation();
		pl.setName("Prop1");
		pl.setAddress("3/21");
		pl.setCity("Bangalore");
		pl.setState("Karnataka");
		assertEquals(daoImpl.checkProp(pl),0);
	}
	@Test
	public void checkPropTest2() {
		PropertyLocation pl=new PropertyLocation();
		pl.setName("abc");
		pl.setAddress("3/25");
		pl.setCity("Bangalore");
		pl.setState("Karnataka");
		assertEquals(daoImpl.checkProp(pl),1);
	}
	
	@Test
	public void deletePropTest() {
		int propId=4;
		assertEquals(daoImpl.delete(propId),1);
	}
	
	@Test
	public void cancelBookingTest() {
		int id=1;
		assertEquals(daoImpl.cancelBooking(id),"Cancelled");
	}
	
	@Test
	public void deleteBookingTest() {
		int bookingId=1;
		try {
			assertEquals(daoImpl.deleteBookingDetails(bookingId),"Booking Deleted");
		} catch (SomethingWentWrongException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void checkEditPropTest() {
		PropertyLocation pl=new PropertyLocation();
		pl.setPropId(6);
		pl.setAddress("3/21");
		pl.setCity("Bangalore");
		pl.setHostId(1);
		pl.setName("Prop1");
		pl.setState("Karnataka");
		assertEquals(daoImpl.checkEditProp(pl),0);
	}
	
	@Test
	public void checkEditPropTest2() {
		PropertyLocation pl=new PropertyLocation();
		pl.setPropId(1);
		pl.setAddress("3/21");
		pl.setCity("Bangalore");
		pl.setHostId(1);
		pl.setName("abc");
		pl.setState("Karnataka");
		assertEquals(daoImpl.checkEditProp(pl),1);
	}
	
	@Test
	public void confirmTest() {
		PropertyLocation pl=new PropertyLocation();
		pl.setAddress("3B");
		pl.setCity("Bangalore");
		pl.setHostId(1);
		pl.setName("Home8");
		pl.setState("Karnataka");
		BookingHistory booking=new BookingHistory();
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		//booking.setBookingId(1);
		booking.setCheckInDate(date);
		booking.setCheckOutDate(date);
		booking.setEmail("himaja@gmail.com");
		booking.setGuests(3);
		booking.setProperty(pl);
		assertEquals(daoImpl.confirmBooking(booking),"Confirmed");
	}
	
	@Test
	public void filterTest() throws MyNiceHomeException {
		Filter filter=new Filter();
		ArrayList<String> al=new ArrayList<String>();
		al.add("WiFi");
		al.add("Food");
		al.add("1000-2000");
		filter.setAl(al);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		filter.setCkin(date);
		filter.setCkot(date);
		filter.setDest("Bangalore");
		filter.setNog("2");
		assertEquals(daoImpl.getFilterList(filter).size(),1);
	}
	
	@Test
	public void filterTest2() throws MyNiceHomeException {
		Filter filter=new Filter();
		ArrayList<String> al=new ArrayList<String>();
		al.add("Food");
		filter.setAl(al);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		filter.setCkin(date);
		filter.setCkot(date);
		filter.setDest("Ooty");
		filter.setNog("2");
		assertEquals(daoImpl.getFilterList(filter).size(),0);
	}
	
	@Test
	public void filterTest3() throws MyNiceHomeException {
		Filter filter=new Filter();
		ArrayList<String> al=new ArrayList<String>();
		al.add("WiFi");
		al.add("Food");
		al.add("AC");
		al.add("500-1000");
		filter.setAl(al);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		filter.setCkin(date);
		filter.setCkot(date);
		filter.setDest("Mangalore");
		filter.setNog("2");
		assertEquals(daoImpl.getFilterList(filter).size(),1);
	}
	@Test
	public void filterTest4() throws MyNiceHomeException {
		Filter filter=new Filter();
		ArrayList<String> al=new ArrayList<String>();
		al.add("WiFi");
		al.add("More than 2000");
		filter.setAl(al);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		filter.setCkin(date);
		filter.setCkot(date);
		filter.setDest("Bangalore");
		filter.setNog("2");
		assertEquals(daoImpl.getFilterList(filter).size(),1);
	}
	@Test
	public void filterTest5() throws MyNiceHomeException {
		Filter filter=new Filter();
		ArrayList<String> al=new ArrayList<String>();
		al.add("WiFi");
		al.add("");
		filter.setAl(al);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		filter.setCkin(date);
		filter.setCkot(date);
		filter.setDest("Bangalore");
		filter.setNog("2");
		assertEquals(daoImpl.getFilterList(filter).size(),2);
	}
	@Test
	public void filterTest6() throws MyNiceHomeException {
		Filter filter=new Filter();
		ArrayList<String> al=new ArrayList<String>();
		al.add("Food");
		al.add("AC");
		al.add("");
		filter.setAl(al);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		filter.setCkin(date);
		filter.setCkot(date);
		filter.setDest("Ooty");
		filter.setNog("2");
		assertEquals(daoImpl.getFilterList(filter).size(),1);
	}
	@Test
	public void filterTest7() throws MyNiceHomeException {
		Filter filter=new Filter();
		ArrayList<String> al=new ArrayList<String>();
		al.add("WiFi");
		al.add("AC");
		al.add("Food");
		al.add("");
		filter.setAl(al);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		filter.setCkin(date);
		filter.setCkot(date);
		filter.setDest("Mangalore");
		filter.setNog("2");
		assertEquals(daoImpl.getFilterList(filter).size(),1);
	}
	
	@Test
	public void filterTest8() throws MyNiceHomeException {
		Filter filter=new Filter();
		ArrayList<String> al=new ArrayList<String>();
		al.add("More than 2000");
		filter.setAl(al);
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		filter.setCkin(date);
		filter.setCkot(date);
		filter.setDest("Bangalore");
		filter.setNog("2");
		assertEquals(daoImpl.getFilterList(filter).size(),1);
	}
	
	@Test
	public void getCityTest() {
		ArrayList<PropertyCity> al=new ArrayList<PropertyCity>();
		PropertyCity city=new PropertyCity();
		city.setCity("Bangalore");
		al.add(city);
		assertEquals(daoImpl.getCity().size(),3);
	}
/////new
	@Test
	public void fetchBookingsTest
	
	@Test
	public void fetchBookingsTest() {
		try {
			assertEquals(daoImpl.fetchBookingDetails(1).size(),2);
			assertEquals(daoImpl.fetchBookingDetails(1).size(),2);
		} catch (SomethingWentWrongException e) {
			System.out.println(e);
		}
	}
	
	
	@Test
	public void deleteBookingDetailsDaoTest()
	{
		try {
			assertEquals(daoImpl.deleteBookingDetails(1),"Booking Deleted");
		} catch (SomethingWentWrongException e) {
			
			
		}	
	}
	@Test
	public void addPackageDetailsTest()
	{
		try {
			assertEquals(daoImpl.addPackageDetails(packageDetails),"SUCCESS");
		} catch (SomethingWentWrongException e) {
			
			
		}
	}
	@Test
	public void fetchMailDataTest()
	{
		int tourId=1;
		try {
			assertEquals(daoImpl.fetchMailData(tourId).getAgencyName(),"Sri Ganesh");
		} catch (SomethingWentWrongException e) {
			
		}
	}
	
	public static MailBodyDetails getMailDetails()
	{
		MailBodyDetails mailBodyDetails=new MailBodyDetails();
		mailBodyDetails.setAgencyName("Sri Ganesh");
		mailBodyDetails.setAddress("Near Kadamba Hotel");
		mailBodyDetails.setCost("50");
		return mailBodyDetails;
		}
	public static PackageDetails packageDetrails()
	{
		PackageDetails pd=new PackageDetails();
		pd.setTourId(1);
		pd.setAddress("Near Kadamba Hotel");
		pd.setCity("Udupi");
		pd.setState("Karnataka");
		pd.setCost(50);
		pd.setName("Sri Nithin");
		pd.setVehicles("3");
		return pd;
		
	}
}