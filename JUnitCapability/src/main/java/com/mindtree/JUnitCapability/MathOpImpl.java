package com.mindtree.JUnitCapability;

public class MathOpImpl implements MathOperations{

	public int add(int a, int b) {
		return a+b;
	}

	public String message(String msg) {
		return msg;
	}

	public boolean isTrueOrFalse() {
		return false;
	}

	public int subtract(int a, int b) {
		return a-b;
	}

}
