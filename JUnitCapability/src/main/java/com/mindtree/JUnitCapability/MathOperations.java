package com.mindtree.JUnitCapability;

public interface MathOperations {
	public int add(int a,int b);
	public int subtract(int a,int b);
	public String message(String msg);
	public boolean isTrueOrFalse();
}
