package com.mindtree.JUnitCapability;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MathOperationTest {
	private static MathOperations math;
	@BeforeClass
	public static void beforeClass()
	{
		math=new MathOpImpl();
		System.out.println("Executes only once even before the test cases start executing");
	}
	@Before
	public void beforeTest()
	{
		System.out.println("Executes before every testcase");
	}
	@After
	public void afterTest()
	{
		System.out.println("Executes after each test case");
	}
	@Test
	public void addTest()
	{
		assertEquals(10,math.add(4, 6));
	}
	@Test
	public void subtractTest()
	{
		assertEquals(4,math.subtract(10, 6));
	}
	@Test
	public void messageTest()
	{
		assertEquals("Hi",math.message("Hi"));
	}
	@Test
	public void isTrueOrFalseTest()
	{
		assertEquals(false,math.isTrueOrFalse());
	}
}
