package com.mindtree.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mindtree.dao.Dao;
import com.mindtree.entity.Book;

@Component
public class ServiceImpl implements Service{
	@Autowired
	Dao dao;
	public void addBook(Book book) {
		
		dao.addBook(book);
	}
	public List<Book> fetchBooks() {
		return dao.fetchBooks();
	}
	public void deleteBooks(int[] bookIds) {
		dao.deleteBooks(bookIds);
		
	}
	
	
	

}
