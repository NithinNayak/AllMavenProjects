package com.mindtree.service;

import java.util.List;

import com.mindtree.entity.Book;

public interface Service {
	public void addBook(Book book);
	public List<Book> fetchBooks();
	public void deleteBooks(int bookIds[]);

}
