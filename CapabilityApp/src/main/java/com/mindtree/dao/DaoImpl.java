package com.mindtree.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.entity.Book;

@Component
public class DaoImpl implements Dao{
	@Autowired
	private SessionFactory sessionFactory;
	@Transactional
	public void addBook(Book book) {
		Session session=sessionFactory.getCurrentSession();
		session.save(book);
		
	}
	@Transactional
	public List<Book> fetchBooks()
	{
		Session session=sessionFactory.getCurrentSession();
		List<Book> books=new ArrayList<Book>();
		Query query = session.createQuery("from Book");
		books = (List<Book> )query.list();
		return books;
	}
	@Transactional
	public void deleteBooks(int[] bookIds) {
		Session session=sessionFactory.getCurrentSession();
		for(int i=0;i<bookIds.length;i++)
		{
			Query query=session.createQuery("from Book b where b.bookId="+bookIds[i]+"");
			Book book=(Book) query.uniqueResult();
			session.delete(book);
			System.out.println(book);
			System.out.println("deleted");
		}
	}
	

}
