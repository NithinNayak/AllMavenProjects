package com.mindtree.MobileMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Hello world!
 *
 */
public class App 
{
	static final String userName = "user name";
	static final String password = "password";
	static final String url = "http://ubaid.tk/sms/sms.aspx";
	static final String charset = "UTF-8";
    public static void main( String[] args ) throws MalformedURLException, IOException
    {
    	
    	URLConnection connection = new URL(url + "?" + buildRequestString("7892006703","Test Mesage")).openConnection();
    	connection.setRequestProperty("Accept-Charset", charset);
    	
    	InputStream response = connection.getInputStream();
    	BufferedReader br = new BufferedReader(new InputStreamReader(response));
    	System.out.println(br.readLine());
    	
    }
    private static String buildRequestString(String targetPhoneNo, String message) throws UnsupportedEncodingException
    {
    	String [] params = new String [5];
    	params[0] = "7892006703";
    	params[1] = "@My9901425";
    	params[2] = message;
    	params[3] = targetPhoneNo;
    	params[4] = "way2sms";
    	String query = String.format("uid=%s&pwd=%s&msg=%s&phone=%s&provider=%s",
    	URLEncoder.encode(params[0],charset),
    	URLEncoder.encode(params[1],charset),
    	URLEncoder.encode(params[2],charset),
    	URLEncoder.encode(params[3],charset),
    	URLEncoder.encode(params[4],charset)
    	);
    	return query;
    }
}
