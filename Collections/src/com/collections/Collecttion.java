package com.collections;

import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Set;

public class Collecttion {

	public static void main(String[] args) {
		
		Set<Object> set1 = new HashSet<Object>();
		Set<Object> set2 = new HashSet<Object>();
		int size = 0;
		Scanner scan = new Scanner(System.in);
		try {
			System.out.println("Enter the set1 size");
			size = scan.nextInt();
			System.out.println("Enter set 1 values");
			scan.nextLine();
			for (int i = 0; i < size; i++) {
				set1.add(scan.nextLine());
			}
			System.out.println("Enter the set2 size");
			size = scan.nextInt();
			System.out.println("Enter set 2 values");
			scan.nextLine();
			for (int i = 0; i < size; i++) {
				set2.add(scan.nextLine());
			}
			System.out.println("Duplicate values are:");
			for (Object o1 : set1) {
				if (set2.contains(o1)) {
					System.out.println(o1);
				}
			}
		} catch (InputMismatchException e) {
			System.out.println("Invalid input");
		} finally {
			scan.close();
		}

	}

}
