package com.employee;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class MainUser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		List<Employee> empList = new ArrayList<Employee>();
		String size = null;
		String mid = null, name = null, address = null;
		Alpha alpha = new Alpha();
		try {
			System.out.println("Enter the number of employees:");
			do {
				size = scan.next();
			} while (!(Pattern.matches("[0-9]+", size)));
			Employee emp[] = new Employee[Integer.parseInt(size)];
			scan.nextLine();
			for (int i = 0; i < Integer.parseInt(size); i++) {
				emp[i] = new Employee();
				System.out.println("Enter the MID");
				do { 
					mid = scan.nextLine();
				} while (!validateMid(mid));
				emp[i].setmId(mid);
				System.out.println("Enter the name");
				do {
					name = scan.nextLine();
				} while (!validateName(name));
				emp[i].setName(name);
				System.out.println("Enter the address");
				do {
					address = scan.nextLine();
				} while (!validateAddress(address));
				emp[i].setAddress(address);
				empList.add(emp[i]);
			}

			Collections.sort(empList, alpha);
			System.out.println("Employee details after sorting:");
			for (Employee e : empList) {
				System.out.println(e);
			}
		} 
		catch(Exception e)
		{
			System.out.println("Invalid input");
		}
		finally {
			scan.close();
		}

	}

	public static boolean validateMid(String mid) {
		if (Pattern.matches("[M][0-9]{7}", mid))
			return true;
		else {
			System.out.println("Invalid mid.Plz enter again");
			return false;
		}

	}

	public static boolean validateName(String name) {
		if (Pattern.matches("[A-Za-z]+", name))
			return true;
		else {
			System.out.println("Invalid name.Plz enter again");
			return false;
		}

	}

	public static boolean validateAddress(String address) {
		if (Pattern.matches("[A-Za-z0-9]+", address))
			return true;
		else {
			System.out.println("Invalid address.Plz enter again");
			return false;
		}

	}

}
