package com.mindtree.travelapp;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class TravelReport {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Connection con = null;
		ResultSet res = null;
		PreparedStatement pstmt = null;
		String dbName="application";
		String usrnme="root";
		String pswrd="Welcome123";
		String destn = "";
		try {
			System.out.println("Enter the destination");
			destn = in.nextLine();
			if (Pattern.matches("[A-Za-z]+", destn) == false || Pattern.matches("[' ']+", destn) == true)//Checks the input for proper format
				throw new InvalidDestinationNameException();//Throw custom exception if destination is invalid
		} catch (InvalidDestinationNameException e) {//Handles the custom exception
			System.out.println(e);
			System.exit(0);
		}
		try {
			Class.forName("com.mysql.jdbc.Driver");//Dynamically loads the Driver class
			System.out.println("Driver loaded");
		} catch (ClassNotFoundException e) {
			System.out.println("Driver not loaded");

		}
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/application", usrnme, pswrd);//Establishes connection with database using the url which points to db
			System.out.println("Connection established");
		} catch (SQLException e) {
			System.out.println("Connection failed");
		}
		try {
			pstmt = con.prepareStatement("SELECT BookingID,Source FROM TravelBooking where Destination=?");//Performs operations on database where ? is a placeholder
			pstmt.setString(1, destn);//Replaces the placeholder by user given input stored in destn
			res = pstmt.executeQuery();//Executes the query and returns a result set
			System.out.println("Query executed");
		} catch (SQLException e) {
			System.out.println("Query not executed");
		}
		try {
			while (res.next()) {//This loop iterates result set row-wise and prints row elements
				System.out.println(res.getInt(1) + " " + res.getString(2));
			}
		} catch (SQLException e) {
			System.out.println("Could not print");
		} finally {//Database connection is closed here
			in.close();
			try {
				con.close();
			} catch (SQLException e) {

			}
		}

	}

}
