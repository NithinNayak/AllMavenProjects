package com.mindtree.productcategories;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
public class FileReading {

	public static void main(String[] args)  {
		ArrayList<List> arr=new ArrayList<List>();
		Scanner scan=new Scanner(System.in);
		String filePath="D:\\products.txt";
		FileReader fr=null;
		BufferedReader br=null;
		String Line=null;
		try
		{
			fr=new FileReader(filePath);
			br=new BufferedReader(fr);
			while((Line=br.readLine())!=null)
			{
				String splitStr[]=Line.split(",");
				int slNo=Integer.parseInt(splitStr[0].trim());
				String product=splitStr[1];
				double price=Double.parseDouble(splitStr[2].trim());
				String category=splitStr[3];
				List li=new List(slNo,product.trim(),price,category.trim());
				arr.add(li);
			}
			Collections.sort(arr);
			System.out.println("Enter the product name");
			String catgry=scan.next();
			for(List x:arr)
			{
				if((x.category).equals(catgry))
					System.out.println(x);
			}
			
		}
		catch(FileNotFoundException e)
		{
			System.out.println("File is not found");
		}
		catch(IOException e)
		{
			System.out.println("Could not read");
		}
		

	}

}
