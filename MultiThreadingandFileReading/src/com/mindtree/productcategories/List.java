package com.mindtree.productcategories;


public class List implements Comparable{
	int slNo;
	String product;
	double price;
	String category;
	List(int slNo,String product,double price,String category)
	{
		this.slNo=slNo;
		this.product=product;
		this.price=price;
		this.category=category;
	}
	public String toString()
	{
		return this.slNo+" "+this.product+" "+this.price+" "+this.category;
	}
	public int compareTo(Object ob)
	{
		if((this.product).compareTo(((List)(ob)).product)>1)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

}
