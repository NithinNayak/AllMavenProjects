package com.mindtree.multithrdaplctn;

import java.util.Scanner;

import com.mindtree.multithrdapp.MyThread;

public class MainClass {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the value of x: ");
		int x=scan.nextInt();
		for (int i = 1; i <= x; i++) {//Creating x number of thread
			MyThread m=new MyThread();
			m.start();
	}
	}
}
