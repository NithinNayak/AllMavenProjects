package com.mindtree.multithrdapp;

public class MyThread extends Thread {
	static int count;
	int y=5;
	synchronized public void run() {//This method will be locked and can be used by only one thread at a time
		count++;
		System.out.println(Thread.currentThread().getName()+" "+count);//Just for reference
		this.notifyAll();
		}

}
