package com.mindtree.multithrdapp;

import java.util.Scanner;

public class MainClass {
	public static void main(String[] args) throws InterruptedException {

		Scanner scan=new Scanner(System.in);
		System.out.println("Enter x:");
		int x=scan.nextInt();
		for (int i = 1; i <= 5; i++) {
			MyThread m = new MyThread();
			 synchronized(m)//Apply lock on current thread(Aquiring the lock of MyThread object)
			{
			m.start();//current thread will call start method which in turn calls the run method and thread will lock run method
			m.wait();
			}
		}
System.out.println(MyThread.count);
	}
	}
