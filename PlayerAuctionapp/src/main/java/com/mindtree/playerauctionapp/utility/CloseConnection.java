package com.mindtree.playerauctionapp.utility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CloseConnection {
	public static void closeDbConnection(Connection con, PreparedStatement pstmt, ResultSet res) throws SQLException {
		if(res!=null)
		{
			res.close();
		}
		if(pstmt!=null)
		{
		pstmt.close();
		}
		if(con!=null)
		{
		con.close();
		}
	}

}
