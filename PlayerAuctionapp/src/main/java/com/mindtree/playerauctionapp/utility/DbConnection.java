package com.mindtree.playerauctionapp.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
	static Connection con = null;
	static String username = "root";
	static String pswrd = "Welcome123";
	static String url = "jdbc:mysql://localhost:3306/application";
	static String drivrClasPath = "com.mysql.jdbc.Driver";

	public Connection dbConnector() throws SQLException, ClassNotFoundException {
		Class.forName(drivrClasPath);
		try {
			con = DriverManager.getConnection(url, username, pswrd);
			return con;
		} catch (Exception e) {
			throw new SQLException("Could not establish the connection with database");
		}
	}

}
