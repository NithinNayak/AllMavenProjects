package com.mindtree.playerauctionapp.exceptions;

public class NotABatsmanException extends Exception {
	public String getMessage() {
		return "Invalid Batsman, please check your input";
	}

}
