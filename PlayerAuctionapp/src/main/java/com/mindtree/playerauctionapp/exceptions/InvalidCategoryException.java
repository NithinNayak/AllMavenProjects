package com.mindtree.playerauctionapp.exceptions;

public class InvalidCategoryException extends Exception {
	public String getMessage() {
		return "Invalid category name please check your input";
	}

}
