package com.mindtree.playerauctionapp.exceptions;

public class InvalidTeamNameException extends Exception {
	public String getMessage() {
		return "Invalid team name, please check your input";
	}

}
