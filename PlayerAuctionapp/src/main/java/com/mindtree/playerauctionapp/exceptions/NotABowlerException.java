package com.mindtree.playerauctionapp.exceptions;

public class NotABowlerException extends Exception {
	public String getMessage() {
		return "Invalid Bowler, please check your input";
	}

}
