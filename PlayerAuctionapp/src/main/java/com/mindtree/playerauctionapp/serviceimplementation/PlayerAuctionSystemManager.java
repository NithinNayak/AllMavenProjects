package com.mindtree.playerauctionapp.serviceimplementation;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.mindtree.playerauctionapp.client.PlayerAuctionSystemClient;
import com.mindtree.playerauctionapp.daoimplementation.DaoImpl;
import com.mindtree.playerauctionapp.entity.PlayerDetails;
import com.mindtree.playerauctionapp.exceptions.DuplicateEntryException;
import com.mindtree.playerauctionapp.exceptions.InvalidCategoryException;
import com.mindtree.playerauctionapp.exceptions.InvalidTeamNameException;
import com.mindtree.playerauctionapp.exceptions.NotABatsmanException;
import com.mindtree.playerauctionapp.exceptions.NotABowlerException;
import com.mindtree.playerauctionapp.service.PlayerAuctionSystem;
import com.mindtree.playerauctionapp.utility.CloseConnection;
import com.mindtree.playerauctionapp.utility.DbConnection;

public class PlayerAuctionSystemManager implements PlayerAuctionSystem {
	DaoImpl d = new DaoImpl();

	public void addAPlayer(PlayerDetails p) {
		Scanner scan = new Scanner(System.in);
		String name = null, category = null, bestFigure = null, teamName = null;
		int highestScore = 0;
		try {
			System.out.print("Enter Player Name: ");
			do {
				name = scan.nextLine();
			} while (!isValidName(name));
			System.out.print("Enter Category: ");
			do {
				category = scan.nextLine();
			} while (!isValidCategory(category));
			validateCategory(category);
			System.out.print("Enter highest score: ");
			highestScore = readHighestScore();
			validateBatsman(category, highestScore);
			System.out.print("Enter best figure(Wkts/Runs): ");
			do {
				bestFigure = scan.nextLine();
				if (bestFigure.isEmpty()) {
					bestFigure = null;
					break;
				}
			} while (!isValidFigure(bestFigure));
			validateBowler(category, bestFigure, highestScore);
			System.out.print("Enter team name: ");
			do {
				teamName = scan.next();
			} while (!isValidTeamName(teamName));
			validateTeamName(teamName);
			checkDuplicateEntry(name, category, teamName);
			p.setName(name);
			p.setCategory(category);
			p.setHighestScore(highestScore);
			p.setBestFigure(bestFigure);
			p.setTeamName(teamName);
			d.insertIntoTable(p);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
			PlayerAuctionSystemClient.main(null);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} catch (InvalidCategoryException e) {
			System.out.println(e.getMessage());
			PlayerAuctionSystemClient.main(null);
		} catch (InvalidTeamNameException e) {
			System.out.println(e.getMessage());
			PlayerAuctionSystemClient.main(null);
		} catch (NotABatsmanException e) {
			System.out.println(e.getMessage());
			PlayerAuctionSystemClient.main(null);
		} catch (NotABowlerException e) {
			System.out.println(e.getMessage());
			PlayerAuctionSystemClient.main(null);
		} catch (DuplicateEntryException e) {
			System.out.println(e.getMessage());
			PlayerAuctionSystemClient.main(null);
		}
	}

	public int readHighestScore() {
		Scanner scan = new Scanner(System.in);
		try {
			int score = scan.nextInt();
			return score;
		} catch (InputMismatchException e) {
			scan.next();
			System.out.println("Invalid input.Enter again");
			readHighestScore();
			return 0;
		}
	}

	public boolean isValidCategory(String category) {
		if (!Pattern.matches("[a-zA-z\\s]+", category)) {
			System.out.println("Entered category is invalid.Please enter a valid category");
			return false;
		} else {
			return true;
		}
	}

	public boolean isValidName(String name) {
		if (!Pattern.matches("[a-zA-z\\s]+", name)) {
			System.out.println("Entered name is invalid.Please enter a valid name");
			return false;
		} else {
			return true;
		}
	}

	public boolean isValidTeamName(String teamName) {
		if (!Pattern.matches("[a-zA-z\\s]+", teamName)) {
			System.out.println("Entered team name is invalid.Please enter a valid team name");
			return false;
		} else {
			return true;
		}
	}

	public boolean isValidFigure(String bestFigure) {
		if (!Pattern.matches("[0-9]/[0-9]+", bestFigure)) {
			System.out.println("Entered data is invalid.Please enter a valid best figure as per the format");
			return false;
		} else {
			return true;
		}
	}

	public void validateCategory(String category) throws InvalidCategoryException {
		if (!category.equalsIgnoreCase("Batsman") && !category.equalsIgnoreCase("Bowler")
				&& !category.equalsIgnoreCase("Allrounder"))
			throw new InvalidCategoryException();

	}

	public void validateTeamName(String teamName)
			throws ClassNotFoundException, SQLException, InvalidTeamNameException {
		if (DaoImpl.isValidTeamName(teamName) == false)
			throw new InvalidTeamNameException();
	}

	public void validateBatsman(String category, int highestScore) throws NotABatsmanException {
		if (category.equalsIgnoreCase("Batsman") && ((highestScore >= 50) == false || (highestScore <= 200) == false))
			throw new NotABatsmanException();
	}

	public void validateBowler(String category, String bestFigure, int highestScore) throws NotABowlerException {
		if (category.equalsIgnoreCase("Bowler") && (bestFigure == null || highestScore < 0))
			throw new NotABowlerException();
	}

	public void checkDuplicateEntry(String name, String category, String teamName)
			throws ClassNotFoundException, SQLException, DuplicateEntryException {
		if (DaoImpl.isDuplicateEntry(name, category, teamName))
			throw new DuplicateEntryException();
	}

	public void displayPlayers(PlayerDetails p) {
		try {
			System.out.println("Enter the team name");
			do {
				Scanner scan = new Scanner(System.in);
				p.setTeamName(scan.next());
			} while (!isValidTeamName(p.getTeamName()));
			validateTeamName(p.getTeamName());
			d.retrieveFromTable(p);
		} catch (InvalidTeamNameException e) {
			System.out.println(e.getMessage());
			PlayerAuctionSystemClient.main(null);
		} catch (SQLException e) {
			System.out.println("No team found with the mentioned name");
		} catch (ClassNotFoundException e) {
			System.out.println("Problem with driver");
		}
	}

}
