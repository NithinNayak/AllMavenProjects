package com.mindtree.playerauctionapp.dao;

import java.sql.SQLException;

import com.mindtree.playerauctionapp.entity.PlayerDetails;

public interface Dao {
	void insertIntoTable(PlayerDetails p) throws ClassNotFoundException, SQLException;

	void retrieveFromTable(PlayerDetails p) throws ClassNotFoundException, SQLException;
}
