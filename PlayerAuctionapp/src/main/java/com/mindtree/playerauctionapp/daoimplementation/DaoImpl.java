package com.mindtree.playerauctionapp.daoimplementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mindtree.playerauctionapp.dao.Dao;
import com.mindtree.playerauctionapp.entity.PlayerDetails;
import com.mindtree.playerauctionapp.utility.CloseConnection;
import com.mindtree.playerauctionapp.utility.DbConnection;

public class DaoImpl implements Dao {
	static Connection con = null;
	static ResultSet res = null;
	static String query = null;
	static PreparedStatement pstmt = null;
	static Statement stmt = null;

	public void insertIntoTable(PlayerDetails p) throws ClassNotFoundException, SQLException {
		int playerNum = 0;
			DbConnection dbConnection=new DbConnection();
			con = dbConnection.dbConnector();
		try {
			query = "INSERT INTO PLAYER_DB.PLAYER(PLAYER_NAME,CATEGORY,HIGHESTSCORE,BESTFIGURE) VALUES(?,?,?,?)";
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, p.getName());
			pstmt.setString(2, p.getCategory());
			pstmt.setInt(3, p.getHighestScore());
			pstmt.setString(4, p.getBestFigure());
			pstmt.executeUpdate();
			pstmt = con.prepareStatement("SELECT MAX(PLAYER_DB.PLAYER.PLAYER_NO) FROM PLAYER_DB.PLAYER");
			res = pstmt.executeQuery();
			while (res.next()) {
				playerNum = res.getInt(1);
			}
			System.out.println("Player added successfully with player No: " + playerNum);
			query = "INSERT INTO PLAYER_DB.TEAM_PLAYER VALUES(?,(SELECT TEAM_ID FROM PLAYER_DB.TEAM WHERE TEAM_NAME=?))";
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, playerNum);
			pstmt.setString(2, p.getTeamName());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			throw new SQLException("Data couldn't be saved due to some reason");
		}
		finally
		{
			CloseConnection.closeDbConnection(con,pstmt,res);
		}

	}

	public void retrieveFromTable(PlayerDetails p) throws ClassNotFoundException, SQLException {
		DbConnection dbConnection=new DbConnection();
		con = dbConnection.dbConnector();
		try {
			query = "select player_db.player.player_name,player_db.player.category from "
					+ "(player_db.team_player join player_db.team "
					+ "on player_db.team_player.team_id=player_db.team.team_id) join player_db.Player on player_db.player.player_no=player_db.team_player.player_no "
					+ "where player_db.team.team_name=? " + "order by player_db.team.team_name";
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, p.getTeamName());
			res = pstmt.executeQuery();
			if (!res.next()) {
				System.out.println("No player present in this team");
			} else {
				System.out.println("Player Name      Category");
				System.out.println("-----------------------");
				res.previous();
				while (res.next()) {
					System.out.println(res.getString(1) + "         " + res.getString(2));
				}
			}
		} catch (SQLException e) {
			throw new SQLException("No data found with relevent search");
		}
		finally
		{
			CloseConnection.closeDbConnection(con,pstmt,res);
		}
	}

	public static boolean isValidTeamName(String teamName) throws ClassNotFoundException, SQLException {
		
		DbConnection dbConnection=new DbConnection();
		con = dbConnection.dbConnector();
		try {
			query = "SELECT TEAM_NAME FROM PLAYER_DB.TEAM";
			pstmt = con.prepareStatement(query);
			res = pstmt.executeQuery();
			while (res.next()) {
				if (teamName.equals(res.getString(1)))
					return true;
			}
			return false;
		} catch (SQLException e) {
			throw new SQLException("Not a valid team name");
		}
		finally
		{
			CloseConnection.closeDbConnection(con,pstmt,res);
		}

	}

	public static boolean isDuplicateEntry(String name, String category, String teamName)
			throws ClassNotFoundException, SQLException {
		
		DbConnection dbConnection=new DbConnection();
		con = dbConnection.dbConnector();
		try {
			query = "select player.player_name,player.category,team.team_name from (player_db.team_player join player_db.team on team_player.team_id=team.team_id) join player_db.Player on player.player_no=team_player.player_no";
			pstmt = con.prepareStatement(query);
			res = pstmt.executeQuery();
			while (res.next()) {
				if (name.equals(res.getString(1)) && category.equals(res.getString(2))
						&& teamName.equals(res.getString(3)))
					return true;
			}
			return false;
		} catch (SQLException e) {
			throw new SQLException("Couldn't check for duplications");
		}
		finally
		{
			CloseConnection.closeDbConnection(con,pstmt,res);
		}
	}

}
