package com.mindtree.playerauctionapp.service;

import com.mindtree.playerauctionapp.entity.PlayerDetails;

public interface PlayerAuctionSystem {
	void addAPlayer(PlayerDetails p);

	void displayPlayers(PlayerDetails p);

}
