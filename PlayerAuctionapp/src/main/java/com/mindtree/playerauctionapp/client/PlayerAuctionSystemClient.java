package com.mindtree.playerauctionapp.client;

import java.util.Scanner;

import com.mindtree.playerauctionapp.entity.PlayerDetails;
import com.mindtree.playerauctionapp.serviceimplementation.PlayerAuctionSystemManager;

public class PlayerAuctionSystemClient {
	static String input="0";
	static boolean active=true;
	static PlayerAuctionSystemManager pasm = new PlayerAuctionSystemManager();
	static PlayerDetails pd = new PlayerDetails();

	public static void main(String[] args) {
		while (active) {
			Scanner scan = new Scanner(System.in);
			if (Integer.parseInt(input) == 0) {
				System.out.println("Enter 1 for Adding Player");
				System.out.println("Enter 2 for Display Players");
				System.out.println("Enter 3 for exit");
				input = scan.next();
			}
			switch (Integer.parseInt(input)) {
			case 1:
				System.out.println("Add Player!!!");
				System.out.println("-----------------------");
				pasm.addAPlayer(pd);
				break;
			case 2:
				System.out.println("Display Players!!!");
				System.out.println("-----------------------");
				pasm.displayPlayers(pd);
				break;
			case 3:
				System.out.println("You have been successfully exited from the app");
				active=false;
			default:
				System.out.println("You have entered a wrong choice");
				break;
			}
			input = "0";
			scan.nextLine();
		}

	}

}
