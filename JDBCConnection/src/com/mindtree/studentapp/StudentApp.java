package com.mindtree.studentapp;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class StudentApp {
	public static void main(String[]args)
	{
		Connection con=null;
		Statement stmt=null;
		PreparedStatement pstmt=null;
		CallableStatement callStmt=null;
		ResultSet res=null;
		Scanner scan=new Scanner(System.in);
		try{
		Class.forName("com.mysql.jdbc.Driver");
		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/stdnt","root","Welcome123");
		}
		catch(SQLException e)
		{
			System.out.println("Driver not found");
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("Couldn't establish connection");
		}
		try
		{
			//Prepare Statement
		pstmt=con.prepareStatement("INSERT INTO STUD_DET VALUES(?,?,?)");
		System.out.println("Enter the id");
		pstmt.setInt(1, scan.nextInt());
		System.out.println("Enter the name");
		pstmt.setString(2,scan.next());
		System.out.println("Enter the marks");
		pstmt.setInt(3, scan.nextInt());
		pstmt.execute();
		System.out.println("Successfully inserted the row");
		
		
		System.out.println("Row present in table is");
		
		//Create Statement
		stmt=con.createStatement();
		res=stmt.executeQuery("SELECT * FROM STUD_DET");
		while(res.next())
		{
			System.out.println(res.getInt(1)+" "+res.getString(2)+" "+res.getInt(3));
		}
		res.close();
		
		//Callable Statement
		callStmt=con.prepareCall("{call stdProc(?)}");
		System.out.println("Enter id");
		callStmt.setInt(1, scan.nextInt());
		res=callStmt.executeQuery();
		while(res.next())
		{
			System.out.println(res.getInt(1)+" "+res.getString(2)+" "+res.getInt(3));
		}
		}
		catch(SQLException e)
		{
			System.out.println("Couldn't fetch data");
		}
		
		finally
		{
			try{
				res.close();
				stmt.close();
				callStmt.close();
				pstmt.close();
			con.close();
			scan.close();
			System.out.println("Connection closed");
			}
			catch(SQLException e)
			{
				System.out.println("Couldn't close connection");
			}
		}
		
		
	}

}
