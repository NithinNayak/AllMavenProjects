package com.mindtree.travelbookapp.client;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.mindtree.travelbookapp.daoimplementation.DaoImpl;
import com.mindtree.travelbookapp.entity.CustomerDetails;
import com.mindtree.travelbookapp.exceptions.InvalidDestinationException;
import com.mindtree.travelbookapp.serviceImplementation.ServiceImpl;
import com.mindtree.travelbookapp.utility.CloseConnections;

public class MainUser {
	static int choice;

	public static void main(String[] args) {
		ServiceImpl servImpl=new ServiceImpl();
		DaoImpl daoImpl=new DaoImpl();
		CustomerDetails c=new CustomerDetails();
		Scanner scan=new Scanner(System.in);
		boolean isValidInput=true;
		try
		{
			while(isValidInput){
				if(choice==0)
				{
				System.out.println("Press 1 to insert the data into table");
				System.out.println("Press 2 to retrieve the data from the table");
				System.out.println("Press any other key to exit the app");				
				choice=scan.nextInt();
				}
				switch(choice){
		case 1:System.out.println("Enter the customer details");
		       servImpl.tableData(c);
		       daoImpl.insert(c);
		       break;
		case 2:System.out.println("Enter the destination");
		       servImpl.inputData();
		       servImpl.validateData();
		       ResultSet res=daoImpl.retrieve();
		       servImpl.displayResult(res);
		       break;
		default:isValidInput=false;       
				}
				choice=0;
			}
		}
		/*catch(IOException e)
		{
			System.out.println(e.getMessage());
		}*/
		catch(InvalidDestinationException e){
			System.out.println(e.getMessage());
			main(args);
		}
		catch(SQLException e){
			System.out.println(e.getMessage());
		}
		catch(ClassNotFoundException e)
		{
			System.out.println("Driver class not found");
		}
		catch(InputMismatchException e)
		{
			System.out.println("Here");
		}
		finally{
			CloseConnections.closeDbConnection(daoImpl.con);
			CloseConnections.closeScannerConnection(scan);
			System.out.println("Thank you for using our service");
		}
		

	}

}