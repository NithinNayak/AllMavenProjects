package com.mindtree.travelbookapp.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.mindtree.travelbookapp.entity.CustomerDetails;
import com.mindtree.travelbookapp.exceptions.InvalidDestinationException;

public interface Service {
	void tableData(CustomerDetails c);
	void validateData() throws InvalidDestinationException;
	void displayResult(ResultSet res) throws SQLException;

}
