package com.mindtree.travelbookapp.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.mindtree.travelbookapp.entity.CustomerDetails;

public interface Dao {
	void insert(CustomerDetails c) throws SQLException,ClassNotFoundException;
	ResultSet retrieve() throws SQLException,ClassNotFoundException;

}
