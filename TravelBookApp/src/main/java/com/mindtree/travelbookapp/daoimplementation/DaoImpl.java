package com.mindtree.travelbookapp.daoimplementation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.mindtree.travelbookapp.dao.Dao;
import com.mindtree.travelbookapp.entity.CustomerDetails;
import com.mindtree.travelbookapp.serviceImplementation.ServiceImpl;
import com.mindtree.travelbookapp.utility.DBConnection;

public class DaoImpl implements Dao{
	Scanner scan=new Scanner(System.in);
	PreparedStatement pstmt=null;
	ResultSet res=null;
	public Connection con=null;
	String query=null;
	DBConnection db=null;
	public void insert(CustomerDetails c) throws SQLException,ClassNotFoundException
	{
		if(con==null)
		{
		db=new DBConnection();
		con=db.dbConnection();
		}
		try{
		query="INSERT INTO APPLICATION.TRAVELBOOKING VALUES(?,?,?,?,?)";
		pstmt=con.prepareStatement(query);
		pstmt.setInt(1, c.getBookingId());
		pstmt.setString(2, c.getSource());
		pstmt.setString(3,c.getDestination());
		pstmt.setString(4, c.getVehicleType());
		pstmt.setString(5, c.getPhoneNo());
		pstmt.executeUpdate();
		}
		catch(Exception e)
		{
			throw new SQLException("Can't execute the query");
		}
	}
	public ResultSet retrieve() throws SQLException,ClassNotFoundException
	{
		if(con==null)
		{
		db=new DBConnection();
		con=db.dbConnection();
		}
		try{
	    query="SELECT BookingID,Source FROM application.TravelBooking where Destination=?";
		pstmt=con.prepareStatement(query);
		pstmt.setString(1,ServiceImpl.destn);
		res=pstmt.executeQuery();
		return res;
		}
		catch(Exception e)
		{
			throw new SQLException("Can't retrieve the booking details");
		}
	}

}
