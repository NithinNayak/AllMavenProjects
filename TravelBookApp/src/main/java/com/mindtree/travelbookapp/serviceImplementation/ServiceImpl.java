package com.mindtree.travelbookapp.serviceImplementation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.mindtree.travelbookapp.entity.CustomerDetails;
import com.mindtree.travelbookapp.exceptions.InvalidDestinationException;
import com.mindtree.travelbookapp.service.Service;

public class ServiceImpl implements Service{
	public static String destn=null;
	Scanner scan=new Scanner(System.in);
	public void tableData(CustomerDetails c)
	{
		System.out.println("Enter the ID");
		c.setBookingId(scan.nextInt());
		System.out.println("Enter the Source");
		c.setSource(scan.next());
		System.out.println("Enter the destination");
		c.setDestination(scan.next());
		System.out.println("Enter the vehicle type");
		c.setVehicleType(scan.next());
		System.out.println("Enter the phone number");
		c.setPhoneNo(scan.next());
	}
	public void inputData()
	{
		destn = scan.nextLine();
	}
	public void validateData() throws InvalidDestinationException
	{
		if (Pattern.matches("[A-Za-z]+", destn) == false || Pattern.matches("[' ']+", destn) == true)
			throw new InvalidDestinationException();
		
	}
	public void displayResult(ResultSet res) throws SQLException
	{
		try{
			if(!res.next())
				System.out.println("No records with mentioned destination");
			res.previous();
		while (res.next()) {
				System.out.println(res.getInt(1) + " " + res.getString(2));
	}
		}
		catch(Exception e)
		{
			throw new SQLException("Error in printing");
		}

}
}
