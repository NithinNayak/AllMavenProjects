package com.mindtree.travelbookapp.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	Connection con=null;
	String username="root";
	String password="Welcome123";
	String url="jdbc:mysql://localhost:3306/application";
	String drivrClasPath="com.mysql.jdbc.Driver";
	public Connection dbConnection() throws ClassNotFoundException,SQLException
	{
	Class.forName(drivrClasPath);
	try
	{
	con = DriverManager.getConnection(url, username, password);
	return con;
	}
	catch(Exception e){
		throw new SQLException("Could not establish the connection with database");
	}
	}

}
