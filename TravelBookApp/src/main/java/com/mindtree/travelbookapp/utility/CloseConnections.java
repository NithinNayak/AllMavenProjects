package com.mindtree.travelbookapp.utility;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class CloseConnections {
	public static void closeDbConnection(Connection con)
	{
		try
		{
			if(con!=null)
		con.close();
		}
		catch(SQLException e)
		{
			System.out.println("Couldn't close the connection");
		}
	}
	public static void closeScannerConnection(Scanner scan)
	{
		scan.close();
	}

}
