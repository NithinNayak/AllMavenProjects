package com.mindtree.travelbookapp.exceptions;

public class InvalidDestinationException extends Exception{
	public String getMessage()
	{
		return "Invalid Destination.Enter a valid destination";
	}

}
